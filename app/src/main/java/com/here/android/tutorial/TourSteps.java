package com.here.android.tutorial;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TourSteps extends AppCompatActivity {

    Integer step, receiverId, id=0;
    Boolean done;
    String one,two,three,oneinfo,twoinfo,threeinfo,newtv,alerttext, tourdata, infotxt,time;
    TextView address;
    JSONArray StopJson;
    JSONObject currentStopJson, iterateJson;
    String coordinates;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("LastActivity","TourSteps").apply();
        done=false;
        setContentView(R.layout.toursteps);
        address=findViewById(R.id.tourinfotxt);
        /*Intent intent=getIntent();
        step=intent.getIntExtra("step",0);
        tourdata=intent.getStringExtra("tourinfos");
        try{
            JSONObject rawtour=new JSONObject(tourdata);
            StopJson=rawtour.getJSONArray("stops");
        }
        catch(JSONException e){
            e.printStackTrace();
        }*/

        stepProgress();

    }

    private void stepProgress(){
        final DatabaseHandler db=new DatabaseHandler(this);
        List<DB_CONTENT_REC> receiverInfoList=db.getNextReceiver();
        if (receiverInfoList.size()>0){
            String lName="", fName="", strName="", strNmbr="",zip="",city="",latitude="",longitude="";
            for (DB_CONTENT_REC dcr : receiverInfoList){
                lName=dcr.getLNAME();
                fName=dcr.getFNAME();
                strName=dcr.getSTRNAME();
                strNmbr=dcr.getSTRNMBR();
                zip=dcr.getZIP();
                city=dcr.getCITY();
                latitude=dcr.getLAT();
                longitude=dcr.getLONG();
                id=dcr.getID();
                time=dcr.getTIMEFRAME();
            }
            infotxt=lName+", "+fName+"\n"+strName+" "+strNmbr+"\n"+zip+" "+city;
            coordinates=latitude+"?"+longitude;
            /*try{

                if(step==0){
                    for (int i=0;i<StopJson.length();i++){
                        JSONObject currentObject=StopJson.getJSONObject(i);
                        if (currentObject.getString("stopType").equals("Depot")){
                            StopJson.remove(i);
                        }
                    }
                }
                if (step==StopJson.length()-1){
                    done=true;
                }
                currentStopJson= StopJson.getJSONObject(step);
                String t=currentStopJson.getString("plannedTimeframeStart");
                time=t.substring(t.lastIndexOf("T") + 1);
                receiverId=Integer.parseInt(currentStopJson.getString("id"));
                infotxt=currentStopJson.getString("lastName")+", "+currentStopJson.getString("firstName")+"\n"+currentStopJson.getString("streetName")+
                        " "+currentStopJson.getString("streetNumber")+"\n"+currentStopJson.getString("zip")+" "+currentStopJson.getString("city");
                coordinates=currentStopJson.getString("latitude")+"?"+currentStopJson.getString("longitude");
            }
            catch (JSONException e){
                Log.e("TourSteps",e.toString());
            }*/
            String information=infotxt+"\n Geplante Zeit: "+time;
            address.setText(information);
        }
        if (receiverInfoList.size()==0){
            new AlertDialog.Builder(TourSteps.this)
                    .setTitle("Lieferungen abgeschlossen")
                    .setMessage("Die Lieferungen wurden erfolgreich bearbeitet.\nDeinem Konto werden 731€ gutgeschrieben!")


                    .setPositiveButton("Toll..", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent=new Intent(TourSteps.this,Start.class);
                            startActivity(intent);
                            finish();
                        }

                    })

                    .show();
        }
    }

    public void showtourlist(View v){
        /*String all="";
        for (int i=0;i<StopJson.length();i++){
            try {
                iterateJson = StopJson.getJSONObject(i);
                all=all+iterateJson.getString("lastName")+", "+iterateJson.getString("firstName")+";"+iterateJson.getString("streetName")+
                        " "+iterateJson.getString("streetNumber")+";"+iterateJson.getString("zip")+" "+iterateJson.getString("city")+"\n";
            }
            catch (JSONException e){
                Log.e("Toursteps", e.toString());
            }
        }
        new AlertDialog.Builder(TourSteps.this)
                .setTitle("Tourliste")
                .setMessage(all)

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }

                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .show();*/
    }

    public void startNavi(View v){
        Intent intent=new Intent(TourSteps.this,Navigation.class);
        intent.putExtra("coordinates",coordinates);
        startActivity(intent);
    }

    public void uebergabe(View v){
        alerttext="";
        /*if (step==1){
            alerttext=one;
            newtv=twoinfo;
        }
        if (step==2){
            alerttext=two;
            newtv=threeinfo;
        }
        if (step==3){
            alerttext=three;
        }*/
        new AlertDialog.Builder(TourSteps.this)
                .setTitle("Paketübergabe")
                .setMessage("Paket übergeben an:\n"+infotxt+"?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //step++;
                        //address.setText(newtv);
                        Intent intent = new Intent (TourSteps.this, PakTransfer.class);
                        //intent.putExtra("step",step);
                        //intent.putExtra("address",alerttext);
                        //intent.putExtra("tourinfos",tourdata);
                        intent.putExtra("receiverId",id);
                        //intent.putExtra("done",done);
                        startActivity(intent);
                        finish();
                        /*Intent inte = new Intent("android.media.action.IMAGE_CAPTURE");
                        startActivity(inte);*/
                    }

                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton("Nein", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }

                })
                .show();
    }

    public void skippacket(View v){
        final DatabaseHandler db=new DatabaseHandler(this);
        db.deleteRec(id.toString());
        stepProgress();
        /*step++;
        if (!done){
            stepProgress();
        }
        if (done){
            new AlertDialog.Builder(TourSteps.this)
                    .setTitle("Lieferungen abgeschlossen")
                    .setMessage("Die Lieferungen wurden erfolgreich bearbeitet.\nDeinem Konto werden 731€ gutgeschrieben!")


                    .setPositiveButton("Toll..", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent=new Intent(TourSteps.this,Start.class);
                            startActivity(intent);
                            finish();
                        }

                    })

                    .show();
        }*/
    }

}
