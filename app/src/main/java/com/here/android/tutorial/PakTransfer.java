package com.here.android.tutorial;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.itkacher.okhttpprofiler.OkHttpProfilerInterceptor;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Route;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class PakTransfer extends AppCompatActivity {
    Integer step, receiverId, packetnumber;
    String address, tourdata;
    private IntentIntegrator qrScanI;
    Boolean done;
    ApolloClient apolloClient;
    private static final String TAG = "PakTransfer";
    ArrayList<String> packetsForReceiverList=new ArrayList<String>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paktransfer);
        packetnumber=0;
        Intent intent=getIntent();
        //step=intent.getIntExtra("step",1);
        //address=intent.getStringExtra("address");
        //tourdata=intent.getStringExtra("tourinfos");
        //done=intent.getBooleanExtra("done", false);
        receiverId=intent.getIntExtra("receiverId",0);
        /*try {
            JSONObject rawtour = new JSONObject(tourdata);
            JSONArray packetsForReceiver=rawtour.getJSONArray("packets");
            for(int i=0;i<packetsForReceiver.length();i++){
                JSONObject packet=packetsForReceiver.getJSONObject(i);
                if (Integer.parseInt(packet.getString("receiverID"))==receiverId){
                    packetsForReceiverList.add(packet.getString("sscc"));
                    //irgendwo diese Liste abarbeiten, im Layout Anzahl der zu übergebenden Pakete anzeigen, jedes Paket einscannen, aber gleiche Unterschrift bei mehreren
                }
            }
        }
        catch(JSONException e){
            e.printStackTrace();
        }*/
        final DatabaseHandler db=new DatabaseHandler(this);
        packetsForReceiverList=db.findPacketsForRID(receiverId);

        //qrScanI=new IntentIntegrator(this);
        apolloClient=ApolloClient.builder()
                .serverUrl("https://smile.goodstag.com/graphql")
                .okHttpClient(
                        new OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS)
                                .writeTimeout(30, TimeUnit.SECONDS)
                                .readTimeout(30, TimeUnit.SECONDS)
                                .addInterceptor(new OkHttpProfilerInterceptor())
                                .authenticator(new Authenticator() {
                                    @androidx.annotation.Nullable
                                    @Override
                                    public Request authenticate(@androidx.annotation.Nullable Route route, okhttp3.Response response) throws IOException {
                                        String credentials= Credentials.basic("smile","sei7ieQuueka");
                                        return response.request().newBuilder().header("Authorization",credentials).build();
                                    }
                                })
                                .build()

                ).build();
    }

    public void scan(View v){

        Intent intent=new Intent(PakTransfer.this,BarcodeScan.class);
        intent.putStringArrayListExtra("packetList",packetsForReceiverList);
        Button button=findViewById(R.id.butsign);
        button.setVisibility(VISIBLE);
        startActivity(intent);
        //qrScanI.initiateScan();

    }

    public void sign(View v){
        Button button=findViewById(R.id.butsign);
        button.setVisibility(INVISIBLE);
        Button button2=findViewById(R.id.butscan);
        button2.setVisibility(INVISIBLE);
        Button button3=findViewById(R.id.butsavequit);
        button3.setVisibility(VISIBLE);
        Button button4=findViewById(R.id.btnqrauth);
        button4.setVisibility(VISIBLE);
        TextView textauth=findViewById(R.id.textauth);
        textauth.setText("Signature");
        RelativeLayout parent = findViewById(R.id.rellayoutdraw);

        MyDrawView myDrawView= new MyDrawView(this);
        parent.addView(myDrawView);
    }

    public void qrauth(View v){
        Intent inte = new Intent("android.media.action.IMAGE_CAPTURE");
        if (inte.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(inte, 1);
        }
    }

    public void savesignature(View v){
        RelativeLayout parent= findViewById(R.id.rellayoutdraw);
        parent.setDrawingCacheEnabled(true);
        Bitmap b = parent.getDrawingCache();
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, 300, 300, true);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        final DatabaseHandler db=new DatabaseHandler(this);
        db.updateSignature(receiverId.toString(),encoded);
        final SignatureMutation signatureMutation=SignatureMutation.builder().packetID("5f11e58a-5b03-4a61-96e6-338a3e1a56c5").sig(encoded).build();
        apolloClient.mutate(signatureMutation).enqueue(new ApolloCall.Callback<SignatureMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<SignatureMutation.Data> response) {
                Log.i(TAG, response.toString());
                String test=response.toString();
                try {
                    if (response.data().updatePacket) {
                        String a="";
                        db.updateSignatureSent(receiverId.toString());
                        Intent intent=new Intent(PakTransfer.this,RestCalls.class);
                        intent.putExtra("call","pakT");
                        intent.putExtra("recId",receiverId.toString());
                        startActivity(intent);
                        finish();
                    }
                    else{
                        Intent intent=new Intent(PakTransfer.this,RestCalls.class);
                        intent.putExtra("call","pakT");
                        intent.putExtra("recId",receiverId);
                        startActivity(intent);
                        finish();
                    }
                }
                catch(NullPointerException e){
                    e.printStackTrace();
                    String a="";
                    Intent intent=new Intent(PakTransfer.this,RestCalls.class);
                    intent.putExtra("call","pakT");
                    intent.putExtra("recId",receiverId);
                    startActivity(intent);
                    finish();
                }
                test=response.toString();
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                Log.e(TAG, e.getMessage(), e);
                String test=e.toString();
                test=e.toString();
                Intent intent=new Intent(PakTransfer.this,RestCalls.class);
                intent.putExtra("call","pakT");
                intent.putExtra("recId",receiverId);
                startActivity(intent);
                finish();
            }
        });


    }

    public void weitertodo(){
        /*new AlertDialog.Builder(PakTransfer.this)
                .setTitle("Lieferungen abgeschlossen")
                .setMessage("Die Lieferungen wurden erfolgreich bearbeitet.\nDeinem Konto werden 731€ gutgeschrieben!")


                .setPositiveButton("Toll..", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent=new Intent(PakTransfer.this,Start.class);
                        startActivity(intent);
                        finish();
                    }

                })

                .show();*/

        new AlertDialog.Builder(PakTransfer.this)
                .setTitle("Übergabe")
                .setMessage("Das Paket wurde erfolgreich übergeben!")

                .setPositiveButton("Weiter", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent=new Intent (PakTransfer.this,TourSteps.class);
                        //intent.putExtra("step",step);
                        //intent.putExtra("tourinfos",tourdata);
                        startActivity(intent);
                        finish();
                    }

                })

                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            if (step>=4){
                weitertodo();
            }
            else{
                Intent intent=new Intent (PakTransfer.this,TourSteps.class);
                intent.putExtra("step",step);
                startActivity(intent);
                finish();
            }
        }
        if (requestCode!=1){
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result != null) {
                if (result.getContents() == null) {
                    Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
                } else {
                    //JSONObject obj = new JSONObject(result.getContents());
                    final String packet=result.getContents();
                    if (packetsForReceiverList.contains(packet)){
                        Toast.makeText(PakTransfer.this, "Paket erfoglreich eingescannt", Toast.LENGTH_SHORT).show();

                        SimpleDateFormat currentDay=new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                        SimpleDateFormat currentTime=new SimpleDateFormat("HH:mm:ss.000Z",Locale.getDefault());
                        String cTime=currentDay.format(new Date())+"T"+currentTime.format(new Date());
                        final DatabaseHandler db=new DatabaseHandler(this);
                        db.updateDeliveryTime(packet, cTime);
                        db.updateDelivery(packet);
                        packetsForReceiverList.remove(packet);
                        if (packetsForReceiverList.size()==0){
                            Button button=findViewById(R.id.butsign);
                            button.setVisibility(VISIBLE);
                        }

                    }
                    else{
                        Toast.makeText(PakTransfer.this,"Ungültiger Code",Toast.LENGTH_LONG).show();
                    }
                    String a="";

                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

}
