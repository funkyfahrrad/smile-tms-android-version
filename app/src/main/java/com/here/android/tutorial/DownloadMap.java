package com.here.android.tutorial;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.here.android.mpa.common.ApplicationContext;
import com.here.android.mpa.common.MapEngine;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapView;
import com.here.android.mpa.odml.MapLoader;
import com.here.android.mpa.odml.MapPackage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DownloadMap extends AppCompatActivity {
    private final static String TAG = DownloadMap.class.getSimpleName();

    private MapView mapView;
    private Map map;

    private ArrayList<MapPackage> currentInstalledMaps;
    private String currentInstalledMapsString;
    private ProgressBar downloadProgressBar;
    private Button back,dlbtn,removebtn,updatebtn;

    // listener for MapLoader
    private MapLoader.Listener mapLoaderHandler = new MapLoader.Listener() {

        @Override
        public void onProgress(int progress) {
            Log.i(TAG, "Progress " + progress + "%");
            downloadProgressBar.setProgress(progress);
        }

        @Override
        public void onInstallationSize(long diskSize, long networkSize) {
            Log.i(TAG, "Map data require " + diskSize);
        }


        @Override
        public void onGetMapPackagesComplete(MapPackage rootMapPackage,
                                             MapLoader.ResultCode resultCode) {
            if (resultCode == MapLoader.ResultCode.OPERATION_SUCCESSFUL) {
                Log.i(TAG, "Map packages received successful: " + rootMapPackage.getTitle());

                currentInstalledMaps = new ArrayList<>(1);
                populateInstalledMaps(rootMapPackage);
            } else {
                Log.e(TAG, "Can't retrieve map packages: " + resultCode.name());
                Toast.makeText(DownloadMap.this,
                        "Error: " + resultCode.name(), Toast.LENGTH_SHORT).show();
                return;
            }

            StringBuilder sb = new StringBuilder();
            for (MapPackage pac : currentInstalledMaps) {
                sb.append(pac.getTitle());
                sb.append("\n");
            }

            currentInstalledMapsString = sb.toString();
        }


        private void populateInstalledMaps(MapPackage pac) {
            // only take installed root package, so if e.g. Germany is installed,
            // don't check for children states
            if (pac.getInstallationState() == MapPackage.InstallationState.INSTALLED) {
                Log.i(TAG, "Installed package found: " + pac.getTitle() + " id " + pac.getId());
                currentInstalledMaps.add(pac);
            } else if (pac.getChildren() != null && pac.getChildren().size() > 0) {
                for (MapPackage p : pac.getChildren()) {
                    populateInstalledMaps(p);
                }
            }
        }


        @Override
        public void onCheckForUpdateComplete(boolean updateAvailable, String currentMapVersion,
                                             String newestMapVersion, MapLoader.ResultCode resultCode) {
            Log.i(TAG, "onCheckForUpdateComplete. Update available: " + updateAvailable +
                    " current version: " + currentMapVersion);

            AlertDialog.Builder builder = new AlertDialog.Builder(
                    DownloadMap.this, R.style.AppCompatAlertDialogStyle);

            builder.setTitle("Map version checked");
            builder.setMessage("Current map version: " + currentMapVersion + "\n\n"
                    + (updateAvailable ? "Update found to " + newestMapVersion : "No update found")
                    + "\n\n" + "Installed maps:\n" + currentInstalledMapsString);
            builder.setNegativeButton("close", null);

            if (updateAvailable) {
                builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.i(TAG, "Update triggered");
                        downloadProgressBar.setProgress(0);
                        downloadProgressBar.setVisibility(View.VISIBLE);
                        MapLoader.getInstance().performMapDataUpdate();
                    }
                });
            }

            builder.show();
        }


        @Override
        public void onPerformMapDataUpdateComplete(MapPackage rootMapPackage,
                                                   MapLoader.ResultCode resultCode) {
            Log.i(TAG, "onPerformMapDataUpdateComplete");
            downloadProgressBar.setVisibility(View.INVISIBLE);
            String message;
            if (resultCode == MapLoader.ResultCode.OPERATION_SUCCESSFUL) {
                message = "Map updated successfully";
            } else {
                message = "Map update failed: " + resultCode.name();
            }
            Toast.makeText(DownloadMap.this, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onInstallMapPackagesComplete(MapPackage rootMapPackage,
                                                 MapLoader.ResultCode resultCode) {
            downloadProgressBar.setVisibility(View.INVISIBLE);
            String message;
            if (resultCode == MapLoader.ResultCode.OPERATION_SUCCESSFUL) {
                message = "Maps installed successfully";
                Log.i(TAG, "Package is installed: "
                        + rootMapPackage.getId() + " " + rootMapPackage.getTitle());
                MapLoader.getInstance().getMapPackages();
                back.setVisibility(View.VISIBLE);
                dlbtn.setVisibility(View.VISIBLE);
                removebtn.setVisibility(View.VISIBLE);
                updatebtn.setVisibility(View.VISIBLE);

            } else {
                message = "Map installation failed: " + resultCode.name();
                Log.e(TAG, "Failed to install package: "
                        + rootMapPackage.getId() + " " + rootMapPackage.getTitle());
            }
            Toast.makeText(DownloadMap.this, message, Toast.LENGTH_SHORT).show();
        }


        @Override
        public void onUninstallMapPackagesComplete(MapPackage rootMapPackage,
                                                   MapLoader.ResultCode resultCode) {
            Log.i(TAG, "onUninstallMapPackagesComplete");
            String message;
            if (resultCode == MapLoader.ResultCode.OPERATION_SUCCESSFUL) {
                message = "Maps removed successfully";
            } else {
                message = "Map removal failed: " + resultCode.name();
            }
            Toast.makeText(DownloadMap.this, message, Toast.LENGTH_SHORT).show();

            // update packages and get installation state
            MapLoader.getInstance().getMapPackages();
        }
    };
    private OnEngineInitListener engineInitHandler = new OnEngineInitListener() {
        @Override
        public void onEngineInitializationCompleted(Error error) {
            if (error == Error.NONE) {
                map = new Map();
                mapView.setMap(map);

                // more map settings
                map.setProjectionMode(Map.Projection.GLOBE);  // globe projection
                map.setExtrudedBuildingsVisible(true);
                map.setLandmarksVisible(false);

                MapLoader.getInstance().addListener(mapLoaderHandler);
                
                MapLoader.getInstance().getMapPackages();
                // update packages and get installation state
            } else {
                Log.e(TAG, "ERROR: Cannot initialize Map Fragment " + error);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_external_map_loader);
        ApplicationContext context=new ApplicationContext(this);
        mapView = findViewById(R.id.ext_mapview);
        downloadProgressBar = findViewById(R.id.ext_progressBar);
        back=findViewById(R.id.ext_back_btn);
        dlbtn=findViewById(R.id.ext_download_btn);
        removebtn=findViewById(R.id.ext_removeall_btn);
        updatebtn=findViewById(R.id.ext_checkForUpdate_btn);
        String diskCacheRoot = Environment.getExternalStorageDirectory().getPath()
                + File.separator + ".isolated-here-maps";
        boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(
                diskCacheRoot,
                "com.here.android.tutorial.MapService");
        if (!success) {

        } else {
            ApplicationContext applicationContext=new ApplicationContext(this);
            MapEngine.getInstance().init(context,engineInitHandler);
            Integer a=1;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        MapEngine.getInstance().onResume();
        if (mapView != null) {
            mapView.onResume();
        }
    }

    @Override
    public void onPause() {
        if (mapView != null) {
            mapView.onPause();
        }
        MapEngine.getInstance().onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        map = null;
        super.onDestroy();
    }

    public void onDownloadButtonClicked(View view) {
        Log.d(TAG, "Downloading new map data...");
        back.setVisibility(View.INVISIBLE);
        dlbtn.setVisibility(View.INVISIBLE);
        removebtn.setVisibility(View.INVISIBLE);
        updatebtn.setVisibility(View.INVISIBLE);
        List<Integer> downloadList = new ArrayList<>(1);
        downloadList.add(120002);// Berlin/Brandenburg id 120002

        downloadProgressBar.setProgress(0);
        downloadProgressBar.setVisibility(View.VISIBLE);
        MapLoader.getInstance().installMapPackages(downloadList);
    }

    public void ext_back(View v){
        finish();
    }

    public void onRemoveAllButtonClicked(View view) {
        Log.d(TAG, "Removing all map data...");

        List<Integer> removalList = new ArrayList<>(1);

        if (currentInstalledMaps == null || currentInstalledMaps.size() <= 0)
            return;

        for (MapPackage pac : currentInstalledMaps)
            removalList.add(pac.getId());

        MapLoader.getInstance().uninstallMapPackages(removalList);
    }

    public void onCheckMapUpdatesButtonClicked(View view) {
        MapLoader.getInstance().checkForMapDataUpdate();
    }
}
