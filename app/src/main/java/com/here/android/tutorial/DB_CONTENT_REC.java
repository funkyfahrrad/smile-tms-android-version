package com.here.android.tutorial;

public class DB_CONTENT_REC {

    int _id;
    String _fname;
    String _lname;
    String _latitude;
    String _longitude;
    String _strname;
    String _strnmbr;
    String _city;
    String _zip;
    String _sttime;
    String _endtime;
    String _timeframe;
    Integer _number;
    String _type;
    String _signature;
    Integer _sigsent;

    public DB_CONTENT_REC(){

    }

    public DB_CONTENT_REC(int id, String fname, String lname, String latitude, String longitude, String strname, String strnmbr, String city, String zip, String sttime, String endtime, String timeframe, Integer number, String type, String signature){
        this._id= id;
        this._fname=fname;
        this._lname=lname;
        this._latitude=latitude;
        this._longitude=longitude;
        this._strname=strname;
        this._strnmbr=strnmbr;
        this._city=city;
        this._zip=zip;
        this._sttime=sttime;
        this._endtime=endtime;
        this._timeframe=timeframe;
        this._number=number;
        this._type=type;
        this._signature=signature;
    }

   public DB_CONTENT_REC(String fname, String lname, String latitude, String longitude, String strname, String strnmbr, String city, String zip, String sttime, String endtime, String timeframe, Integer number, String type){
        this._fname=fname;
        this._lname=lname;
        this._latitude=latitude;
        this._longitude=longitude;
        this._strname=strname;
        this._strnmbr=strnmbr;
        this._city=city;
        this._zip=zip;
        this._sttime=sttime;
        this._endtime=endtime;
        this._timeframe=timeframe;
        this._number=number;
        this._type=type;
    }



    public int getID(){
        return this._id;
    }

    public void setID(int id){ this._id=id; }

    public String getFNAME(){return this._fname;}

    public void setFNAME(String fname){this._fname=fname;}

    public String getLNAME(){return this._lname;}

    public void setLNAME(String lname){this._lname=lname;}

    public String getLAT(){
        return this._latitude;
    }

    public void setLAT(String lat){
        this._latitude=lat;
    }

    public String getLONG(){
        return this._longitude;
    }

    public void setLONG(String longitude){ this._longitude=longitude; }

    public String getSTRNAME(){return this._strname;}

    public void setSTRNAME(String strname){this._strname=strname;}

    public String getSTRNMBR(){return this._strnmbr;}

    public void setSTRNMBR(String strnmbr){this._strnmbr=strnmbr;}

    public String getZIP(){
        return this._zip;
    }

    public void setZIP(String zip){
        this._zip=zip;
    }

    public String getCITY(){ return this._city; }

    public void setCITY(String city){ this._city=city; }

    public String getSTIME(){return this._sttime;}

    public void setSTIME(String stime){this._sttime=stime;}

    public String getETIME(){
        return this._endtime;
    }

    public void setETIME(String etime){
        this._endtime=etime;
    }

    public String getTIMEFRAME(){ return this._timeframe; }

    public void setTIMEFRAME(String timeframe){ this._timeframe=timeframe; }

    public Integer getNUMBER(){ return this._number; }

    public void setNUMBER(Integer number){ this._number=number; }

    public String getTYPE(){return this._type;}

    public void setTYPE(String type){this._type=type;}

    public String getSIGNATURE(){ return this._signature; }

    public void setSIGNATURE(String signature){ this._signature=signature; }

}
