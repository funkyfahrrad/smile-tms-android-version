package com.here.android.tutorial;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RestCalls extends Activity {
    int status;
    ArrayList<String> ssccList=new ArrayList<>();
    String receiverId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restcalls);
        Intent getInt=getIntent();
        String callFrom="service";

        boolean connected;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
        }
        else {
            connected = false;
            Toast.makeText(RestCalls.this,"Internetverbindung nicht vorhanden oder stabil genug",Toast.LENGTH_LONG).show();
        }

        if (getInt.getStringExtra("call")!=null) {
            callFrom = getInt.getStringExtra("call");
        }
        final DatabaseHandler db=new DatabaseHandler(this);
        if (callFrom.equals("pickup")) {
            if (connected){
                List<DB_CONTENT_PAK> packetList = db.getAllPakets();
                for (DB_CONTENT_PAK dcp : packetList) {
                    ssccList.add(dcp.getSSCC());
                    callAPI_PU(dcp.getSSCC(), dcp.getPICKDATE(),1);
                }
            }
            else{
                Intent intent= new Intent(RestCalls.this,TourSteps.class);
                startActivity(intent);
                finish();
            }

        }
        if (callFrom.equals("pakT")){
            if (connected){
                receiverId=getInt.getStringExtra("recId");
                ArrayList<String> packetList=db.findPacketsForRID(Integer.parseInt(receiverId));
                for (String packet:packetList){
                    ssccList.add(packet);
                    callAPI_Del(packet,1);
                }
            }
            else {
                API_Del_Complete();
            }

        }
        if(callFrom.equals("service")){
            if (connected){
                stopService(new Intent(getBaseContext(),ServiceInternetConnection.class));
                reportPickups();
            }
        }

    }


    protected void callAPI_PU(String ssccDB, String timeDB, final int processnmbr){
        final String sscc=ssccDB;
        final String time=timeDB;
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://bpt-lab.org/smile/caz/tms/pick-up-reported",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (status==200){
                            final DatabaseHandler db=new DatabaseHandler(RestCalls.this);
                            db.updatePickup(sscc);
                            ssccList.remove(sscc);
                            if (ssccList.size()==0){
                                if (processnmbr==1){
                                    Intent intent= new Intent(RestCalls.this,TourSteps.class);
                                    startActivity(intent);
                                    finish();
                                }
                                if (processnmbr==2){
                                    reportDeliveries();
                                }
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(RestCalls.this,error.toString(),Toast.LENGTH_LONG).show();
                        if (ssccList.size()==0){
                            Intent intent= new Intent(RestCalls.this,TourSteps.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                }){

            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("sscc", sscc);
                params.put("pickDate", time);
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                status = response.statusCode;
                return super.parseNetworkResponse(response);
            }

        };
        requestQueue.add(stringRequest);
        // Toast.makeText(getApplicationContext(), "done", Toast.LENGTH_LONG).show();

    }

    protected void callAPI_Del(String ssccDB, final int processnmbr){
        final DatabaseHandler db=new DatabaseHandler(RestCalls.this);
        final String sscc=ssccDB;
        final String time=db.getTimeForSSCC(sscc);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://bpt-lab.org/smile/caz/tms/delivery-reported",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (status==200){
                            db.updateDelivery(sscc);
                            ssccList.remove(sscc);
                            if (ssccList.size()==0){
                                if (processnmbr==1){
                                    API_Del_Complete();
                                }
                                if (processnmbr==2){
                                    reportSignatures();
                                }
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(RestCalls.this,error.toString(),Toast.LENGTH_LONG).show();
                        ssccList.remove(sscc);
                        if (ssccList.size()==0){
                            API_Del_Complete();
                        }
                    }
                }){

            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("sscc", sscc);
                params.put("pickDate", time);
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                status = response.statusCode;
                return super.parseNetworkResponse(response);
            }

        };
        requestQueue.add(stringRequest);
        // Toast.makeText(getApplicationContext(), "done", Toast.LENGTH_LONG).show();

    }

    public void API_Del_Complete(){
        final DatabaseHandler db=new DatabaseHandler(this);
        if (db.checkForMoreReceivers()==0) {
            checkForSentData(); //TODO
        }
        if (db.checkForMoreReceivers()>0){
            Intent intent=new Intent(RestCalls.this,TourSteps.class);
            startActivity(intent);
            finish();
        }
    }

    private void checkForSentData(){
        final DatabaseHandler db=new DatabaseHandler(this);
        if (db.checkSentData() > 0) {
            new AlertDialog.Builder(RestCalls.this)
                    .setTitle("Tour konnte nicht abgeschlossen werden")
                    .setMessage("Leider konnten nicht alle Daten übermittelt werden. Stelle sicher, dass du eine stabile Internetverbindung hast. Dann kann die Tour abgeschlossen werden!")


                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            PreferenceManager.getDefaultSharedPreferences(RestCalls.this).edit().putBoolean("CompletedTourSent",false).apply();
                            Intent intent = new Intent(RestCalls.this, LoginScreen.class);
                            startActivity(intent);
                            finish();
                        }

                    })

                    .show();
        }
        if (db.checkSentData() == 0) {
            PreferenceManager.getDefaultSharedPreferences(this).edit().putString("LastActivity","LoginScreen").apply();
            PreferenceManager.getDefaultSharedPreferences(RestCalls.this).edit().putBoolean("CompletedTourSent",true).apply();
            new AlertDialog.Builder(RestCalls.this)
                    .setTitle("Tour erfolgreich abgeschlossen!")
                    .setMessage("Du hast deine Tour erfolgreich abgeschlossen!")


                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(RestCalls.this, LoginScreen.class);
                            startActivity(intent);
                            finish();
                        }

                    })

                    .show();
        }
    }

   /* private void checkInternet(){
        boolean connected;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
        }
        else {
            connected = false;}
        if (connected){
            reportPickups();
        }
        else{
            Toast.makeText(RestCalls.this,"Internetverbindung nicht vorhanden oder stabil genug, probiere es später erneut",Toast.LENGTH_LONG).show();
        }

    }*/

    private void reportPickups(){
        ssccList.clear();
        final DatabaseHandler db=new DatabaseHandler(this);
        List<DB_CONTENT_PAK> packetList = db.PickUpRepFail();
        if (packetList.size()>0){
            for (DB_CONTENT_PAK dcp : packetList) {
                ssccList.add(dcp.getSSCC());
                callAPI_PU(dcp.getSSCC(), dcp.getPICKDATE(),2);
            }
        }
        else{
            reportDeliveries();
        }

    }

    private void reportDeliveries(){
        ssccList.clear();
        final DatabaseHandler db=new DatabaseHandler(this);
        List<DB_CONTENT_PAK> packetList = db.DelRepFail();
        if (packetList.size()>0){
            for (DB_CONTENT_PAK dcp : packetList) {
                ssccList.add(dcp.getSSCC());
                callAPI_Del(dcp.getSSCC(),2);
            }
        }
        else{
            reportSignatures();
        }

    }

    private void reportSignatures(){
        checkForSentData();
    }



    private void clearDB(){
        final DatabaseHandler db=new DatabaseHandler(this);


    }


}
