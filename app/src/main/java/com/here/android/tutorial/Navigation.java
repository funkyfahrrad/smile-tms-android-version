package com.here.android.tutorial;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

public class Navigation extends AppCompatActivity {
    private Integer step;
    private MapFragmentView m_mapFragmentView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent=getIntent();
        String coordinates=intent.getStringExtra("coordinates");
        setContentView(R.layout.activity_main);
        m_mapFragmentView = new MapFragmentView(this,coordinates);
    }

    public void naviback(View v){
        m_mapFragmentView.destroy();
        finish();
    }
}
