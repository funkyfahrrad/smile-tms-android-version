package com.here.android.tutorial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DatabaseExec extends Activity {

    ArrayList<Integer> testlist=new ArrayList<>();
    Integer j;
    String tourStartTime;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent getIntent=getIntent();
        tourStartTime=getIntent.getStringExtra("time");
        j=1;
        final DatabaseHandler db=new DatabaseHandler(this);
        //db.dropAllTables();
        this.deleteDatabase("DBEventQueue");
        String jsonTour=getIntent.getStringExtra("tourinfo");
        try{
            JSONObject rawTour=new JSONObject(jsonTour);
            JSONArray allStops=rawTour.getJSONArray("stops");
            String a="";
            for (int i=0;i<allStops.length();i++){
                JSONObject jsonTemp=allStops.getJSONObject(i);
                if (jsonTemp.getString("stopType").equals("Receiver")){
                    int id=Integer.parseInt(jsonTemp.getString("id"));
                    String fname=jsonTemp.getString("firstName");
                    String lname=jsonTemp.getString("lastName");;
                    String lat=jsonTemp.getString("latitude");;
                    String longi=jsonTemp.getString("longitude");;
                    String strname=jsonTemp.getString("streetName");;
                    String strnmbr=jsonTemp.getString("streetNumber");;
                    String city=jsonTemp.getString("city");;
                    String zip=jsonTemp.getString("zip");;
                    String stime=jsonTemp.getString("startTime");;
                    String etime=jsonTemp.getString("endTime");;
                    String timeframe=jsonTemp.getString("plannedTimeframeStart");
                    String type="Receiver";
                    String signature="0";
                    Integer number=j;
                    DB_CONTENT_REC dbr=new DB_CONTENT_REC(id, fname,lname,lat,longi,strname,strnmbr,city, zip,stime,etime,timeframe, number,type,signature);
                    db.addReceiver(dbr);
                    j++;
                }
                if (jsonTemp.getString("stopType").equals("Depot")){
                    String idTemp=jsonTemp.getString("id");
                    idTemp=idTemp.substring(idTemp.lastIndexOf(" ")+1);
                    int id=Integer.parseInt(idTemp);
                    String fname="null";
                    String lname="null";
                    if (jsonTemp.getString("firstName")!=null){
                        fname=jsonTemp.getString("firstName");
                    }
                    if (jsonTemp.getString("lastName")!=null){
                        lname=jsonTemp.getString("lastName");
                    }
                    String lat=jsonTemp.getString("latitude");;
                    String longi=jsonTemp.getString("longitude");;
                    String strname=jsonTemp.getString("streetName");;
                    String strnmbr=jsonTemp.getString("streetNumber");;
                    String city=jsonTemp.getString("city");;
                    String zip=jsonTemp.getString("zip");;
                    String stime=tourStartTime;
                    String etime="null";
                    String timeframe="null";
                    String type="Depot";
                    String signature="";
                    Integer number=0;
                    DB_CONTENT_REC dbr=new DB_CONTENT_REC(id, fname,lname,lat,longi,strname,strnmbr,city, zip,stime,etime,timeframe, number,type,signature);
                    db.addReceiver(dbr);
                }
            }
            JSONArray allPackets=rawTour.getJSONArray("packets");
            for (int i=0;i<allPackets.length();i++){
                JSONObject jsonTemp=allPackets.getJSONObject(i);
                int receiverId=Integer.parseInt(jsonTemp.getString("receiverID"));
                String sscc=jsonTemp.getString("sscc");
                sscc=sscc.substring(sscc.length()-18);
                sscc=sscc.replace(".","");
                String receivedate="n/a";
                String pickdate="n/a";
                int pickup=0;
                int delivery=0;
                int scanned=0;
                int sigsent=1;
                DB_CONTENT_PAK dbp=new DB_CONTENT_PAK(sscc,receiverId,pickdate,receivedate,pickup,delivery,scanned, sigsent);
                db.addPaket(dbp);
            }
            Intent intent=new Intent(DatabaseExec.this,Tour.class);
            intent.putExtra("tourinfo",jsonTour);
            startActivity(intent);
            finish();
        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }
}
