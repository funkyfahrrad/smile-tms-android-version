package com.here.android.tutorial;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class TourSuche extends AppCompatActivity {


    Integer zeit, pakete, stops, km,i, arraylength;
    JSONObject receiveinfo, tour;
    String price, noofstops,esttime,starttime, tourStartTime;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toursuche);
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("LastActivity","TourSuche").apply();
        i=0;
        /*zeit=14;
        pakete=3;
        stops=3;
        km=10;*/
    }

    public void toursuche(View v){
        String json=getResources().getString(R.string.json);
        try {
            receiveinfo = new JSONObject(json);
            JSONArray keys = receiveinfo.getJSONArray("tours");
            arraylength=keys.length();
            JSONObject toureinzeln = keys.getJSONObject(i);
            //JSONObject keys2=keys.getJSONObject(0);
            makeAlert(toureinzeln);
            String a = "";
        }
        catch(JSONException e){
            e.printStackTrace();
            Toast.makeText(TourSuche.this,"Fehler, bitte noch einmal probieren",Toast.LENGTH_LONG).show();
        }
      // API NACHFOLGEND
        /*
        AsyncHttpClient client = new AsyncHttpClient();
        client.setBasicAuth(getApplicationContext().getResources().getString(R.string.gtapiuser), getApplicationContext().getResources().getString(R.string.gtapipass));
        client.get("https://bpt-lab.org/smile/sphinx/getTours", null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] bytes) {
                String json = new String(bytes); // This is the json.
                try {
                    receiveinfo = new JSONObject(new String(json));
                    JSONArray keys = receiveinfo.getJSONArray ("tours");

                    for (int i = 0; i < keys.length (); ++i) {
                        final JSONObject toureinzeln=keys.getJSONObject(0);
                        //JSONObject keys2=keys.getJSONObject(0);
                        JSONObject tmetadata=toureinzeln.getJSONObject("tourMetaData");
                        noofstops=tmetadata.getString("numberOfStops");
                        esttime=tmetadata.getString("estimatedTime");
                        String dtStart=tmetadata.getString("tourStartTime");
                        starttime=dtStart.substring(dtStart.lastIndexOf("T") + 1);


                        new AlertDialog.Builder(TourSuche.this)
                                .setTitle("Neue Tour gefunden")
                                .setMessage("Startzeit: "+starttime+" Uhr \n"+noofstops+" Stops \n"+esttime+"h geschätzte Dauer \n"+price+" Gehalt")

                                .setPositiveButton("Annehmen", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent=new Intent(TourSuche.this,Tour.class);
                                        intent.putExtra("tourinfo",toureinzeln.toString());
                                        startActivity(intent);
                                        finish();
                                    }

                                })

                                // A null listener allows the button to dismiss the dialog and take no further action.
                                .setNegativeButton("Ablehnen", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                    }

                                })
                                .show();

                    }
                    String a="";
                }
                catch(JSONException e){
                    e.printStackTrace();
                    Toast.makeText(TourSuche.this,"Fehler, bitte noch einmal probieren",Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable throwable) {
                Toast.makeText(TourSuche.this,"Fehler bei Verbindung mit Server",Toast.LENGTH_LONG).show();
            }
        });*/
    }

    private void makeAlert(JSONObject toureinzeln){
        tour=toureinzeln;
        try {
            JSONObject tmetadata = tour.getJSONObject("tourMetaData");
            noofstops = tmetadata.getString("numberOfStops");
            esttime = tmetadata.getString("estimatedTime");
            price = tmetadata.getString("price");
            tourStartTime = tmetadata.getString("tourStartTime");
            starttime = tourStartTime.substring(tourStartTime.lastIndexOf("T") + 1);
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        new AlertDialog.Builder(TourSuche.this)
                .setTitle("Neue Tour gefunden")
                .setMessage("Startzeit: " + starttime + " Uhr \n" + noofstops + " Stops \n" + esttime + "h geschätzte Dauer \n" + price + " €   ")

                .setPositiveButton("Annehmen", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(TourSuche.this, DatabaseExec.class);
                        intent.putExtra("tourinfo", tour.toString());
                        intent.putExtra("time",tourStartTime);
                        startActivity(intent);
                        finish();
                    }

                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton("Ablehnen", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (i+1<arraylength){
                            i++;
                            toursuche(null);
                        }
                        else{
                            Toast.makeText(TourSuche.this,"Keine weiteren Touren gefunden!",Toast.LENGTH_LONG).show();
                            finish();
                        }
                    }

                })
                .show();
    }
}
