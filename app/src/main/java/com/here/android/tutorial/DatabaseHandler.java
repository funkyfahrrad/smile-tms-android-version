package com.here.android.tutorial;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.here.android.mpa.venues3d.Content;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="DBEventQueue";
    private static final String TABLE_PAK_NAME="PaketData";
    private static final String TABLE_REC_NAME="ReceiverData";

    private static final String KEY_PAK_ID="id";
    private static final String KEY_PAK_SSCC="sscc";
    private static final String KEY_PAK_RECID="receiver_id";
    private static final String KEY_PAK_PICKDATE="pick_date";
    private static final String KEY_PAK_RECEIVEDATE="receive_date";
    private static final String KEY_PAK_PICKUPREPORTED="pickup_reported";
    private static final String KEY_PAK_DELIVERYREPORTED="delivery_reported";
    private static final String KEY_PAK_SCANNED="scanned";
    private static final String KEY_PAK_SIGSENT="signature_sent";

    private static final String KEY_REC_ID="id";
    private static final String KEY_REC_FNAME="first_name";
    private static final String KEY_REC_LNAME="last_name";
    private static final String KEY_REC_LATITUDE="latitude";
    private static final String KEY_REC_LONGITUDE="longitude";
    private static final String KEY_REC_STRNAME="street_name";
    private static final String KEY_REC_STRNMBR="street_number";
    private static final String KEY_REC_CITY="city";
    private static final String KEY_REC_ZIP="zip";
    private static final String KEY_REC_STIME="start_time";
    private static final String KEY_REC_ETIME="end_time";
    private static final String KEY_REC_TIMEFRAME="timeframe";
    private static final String KEY_REC_NUMBER="number";
    private static final String KEY_REC_TYPE="stoptype";
    private static final String KEY_REC_SIG="signature";




    public DatabaseHandler(Context context){
        super(context, DATABASE_NAME,null, DATABASE_VERSION);
    }

    public  void onCreate(SQLiteDatabase db){
        String CREATE_PAK_TABLE="CREATE TABLE "+TABLE_PAK_NAME+"("+KEY_PAK_ID+" INTEGER PRIMARY KEY,"+KEY_PAK_SSCC+" TEXT,"+KEY_PAK_RECID+" INTEGER,"+KEY_PAK_PICKDATE+" TEXT,"+KEY_PAK_RECEIVEDATE
                +" TEXT,"+KEY_PAK_PICKUPREPORTED+" INTEGER,"+KEY_PAK_DELIVERYREPORTED+" INTEGER,"+KEY_PAK_SCANNED+" INTEGER,"+KEY_PAK_SIGSENT+" INTEGER);";
        String CREATE_REC_TABLE="CREATE TABLE "+TABLE_REC_NAME+"("+KEY_REC_ID+" INTEGER PRIMARY KEY,"+KEY_REC_FNAME+" TEXT,"+KEY_REC_LNAME+" TEXT,"+KEY_REC_LATITUDE+" TEXT,"+KEY_REC_LONGITUDE+
                " TEXT,"+KEY_REC_STRNAME+" TEXT,"+KEY_REC_STRNMBR+" TEXT,"+KEY_REC_CITY+" TEXT,"+KEY_REC_ZIP+" TEXT,"+KEY_REC_STIME+" TEXT,"+KEY_REC_ETIME+" TEXT,"+KEY_REC_TIMEFRAME
                +" TEXT,"+KEY_REC_NUMBER+" INTEGER,"+KEY_REC_TYPE+" TEXT,"+KEY_REC_SIG+" TEXT);";

        db.execSQL(CREATE_PAK_TABLE);
        db.execSQL(CREATE_REC_TABLE);

    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_PAK_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_REC_NAME);


        onCreate(db);
    }

    void addPaket(DB_CONTENT_PAK content){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values=new ContentValues();
        values.put(KEY_PAK_SSCC,content.getSSCC());
        values.put(KEY_PAK_RECID,content.getRECID());
        values.put(KEY_PAK_PICKDATE,content.getPICKDATE());
        values.put(KEY_PAK_RECEIVEDATE,content.getRECDATE());
        values.put(KEY_PAK_PICKUPREPORTED,content.getPICKUP());
        values.put(KEY_PAK_DELIVERYREPORTED,content.getDELIVERY());
        values.put(KEY_PAK_SCANNED,content.getSCANNED());
        values.put(KEY_PAK_SIGSENT,content.getSIGSENT());
        //write values into ContentValues and then into DB

        //TODO SCANNED VALUE EVERYWHERE
        db.insert(TABLE_PAK_NAME,null,values);
        //db.close();
    }

    void addReceiver(DB_CONTENT_REC content){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values=new ContentValues();
        values.put(KEY_REC_ID,content.getID());
        values.put(KEY_REC_FNAME,content.getFNAME());
        values.put(KEY_REC_LNAME,content.getLNAME());
        values.put(KEY_REC_LATITUDE,content.getLAT());
        values.put(KEY_REC_LONGITUDE,content.getLONG());
        values.put(KEY_REC_STRNAME,content.getSTRNAME());
        values.put(KEY_REC_STRNMBR,content.getSTRNMBR());
        values.put(KEY_REC_CITY,content.getCITY());
        values.put(KEY_REC_ZIP,content.getZIP());
        values.put(KEY_REC_STIME,content.getSTIME());
        values.put(KEY_REC_ETIME,content.getETIME());
        values.put(KEY_REC_TIMEFRAME,content.getTIMEFRAME());
        values.put(KEY_REC_NUMBER,content.getNUMBER());
        values.put(KEY_REC_TYPE,content.getTYPE());
        values.put(KEY_REC_SIG,content.getSIGNATURE());

        //write values into ContentValues and then into DB
        db.insert(TABLE_REC_NAME,null,values);
        //db.close();
    }

    public List<DB_CONTENT_PAK> getAllPakets(){
        List<DB_CONTENT_PAK> contentList=new ArrayList<DB_CONTENT_PAK>();
        String selectQuery="SELECT * FROM "+TABLE_PAK_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        //cursor based request to DB

        if (cursor.moveToFirst()){
            do{
                DB_CONTENT_PAK content = new DB_CONTENT_PAK();
                content.setID(Integer.parseInt(cursor.getString(0)));
                content.setSSCC(cursor.getString(1));
                content.setRECID(Integer.parseInt(cursor.getString(2)));
                content.setPICKDATE(cursor.getString(3));
                content.setRECDATE(cursor.getString(4));
                content.setPICKUP(Integer.parseInt(cursor.getString(5)));
                content.setDELIVERY(Integer.parseInt(cursor.getString(6)));
                content.setSCANNED(Integer.parseInt(cursor.getString(7)));
                content.setSIGSENT(Integer.parseInt(cursor.getString(8)));

                contentList.add(content);

            } while (cursor.moveToNext());
        }
        cursor.close();
        //db.close();
        return contentList;
    }

    public List<DB_CONTENT_REC> getAllCoordinates(){
        List<DB_CONTENT_REC> contentList=new ArrayList<DB_CONTENT_REC>();
        String selectQuery="SELECT * FROM "+TABLE_REC_NAME+ " ORDER BY number";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);

        if (cursor.moveToFirst()){
            do{
                DB_CONTENT_REC content = new DB_CONTENT_REC();
                content.setID(Integer.parseInt(cursor.getString(0)));
                content.setFNAME(cursor.getString(1));
                content.setLNAME(cursor.getString(2));
                content.setLAT(cursor.getString(3));
                content.setLONG(cursor.getString(4));
                content.setSTRNAME(cursor.getString(5));
                content.setSTRNMBR(cursor.getString(6));
                content.setCITY(cursor.getString(7));
                content.setZIP(cursor.getString(8));
                content.setSTIME(cursor.getString(9));
                content.setETIME(cursor.getString(10));
                content.setTIMEFRAME(cursor.getString(11));
                content.setNUMBER(cursor.getInt(12));
                content.setTYPE(cursor.getString(13));
                content.setSIGNATURE(cursor.getString(14));

                contentList.add(content);

            } while (cursor.moveToNext());
        }
        cursor.close();
        //db.close();
        return contentList;
    }


    //DB Request for getting all packets that haven't been scanned yet
    public List<DB_CONTENT_PAK> getPickupPackets(){
        List<DB_CONTENT_PAK> contentList=new ArrayList<>();
        String selectQuery="SELECT * FROM "+TABLE_PAK_NAME+ " WHERE "+KEY_PAK_SCANNED+"=0;";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);

        if (cursor.moveToFirst()){
            do{
                DB_CONTENT_PAK content = new DB_CONTENT_PAK();
                content.setID(Integer.parseInt(cursor.getString(0)));
                content.setSSCC(cursor.getString(1));
                content.setRECID(Integer.parseInt(cursor.getString(2)));
                content.setPICKDATE(cursor.getString(3));
                content.setRECDATE(cursor.getString(4));
                content.setPICKUP(Integer.parseInt(cursor.getString(5)));
                content.setDELIVERY(Integer.parseInt(cursor.getString(6)));
                content.setSCANNED(Integer.parseInt(cursor.getString(7)));
                content.setSIGSENT(Integer.parseInt(cursor.getString(8)));
                contentList.add(content);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return contentList;
    }

    public List<DB_CONTENT_REC> getAllReceivers(){
        List<DB_CONTENT_REC> contentList=new ArrayList<DB_CONTENT_REC>();
        String selectQuery="SELECT * FROM "+TABLE_REC_NAME+ " WHERE "+KEY_REC_TYPE+"=\"Receiver\" ORDER BY number";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        //cursor based request to DB

        if (cursor.moveToFirst()){
            do{
                DB_CONTENT_REC content = new DB_CONTENT_REC();
                content.setID(Integer.parseInt(cursor.getString(0)));
                content.setFNAME(cursor.getString(1));
                content.setLNAME(cursor.getString(2));
                content.setLAT(cursor.getString(3));
                content.setLONG(cursor.getString(4));
                content.setSTRNAME(cursor.getString(5));
                content.setSTRNMBR(cursor.getString(6));
                content.setCITY(cursor.getString(7));
                content.setZIP(cursor.getString(8));
                content.setSTIME(cursor.getString(9));
                content.setETIME(cursor.getString(10));
                content.setTIMEFRAME(cursor.getString(11));
                content.setNUMBER(cursor.getInt(12));
                content.setTYPE(cursor.getString(13));
                content.setSIGNATURE(cursor.getString(14));

                contentList.add(content);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return contentList;
    }

    public List<DB_CONTENT_REC> getDepot(){
        List<DB_CONTENT_REC> contentList=new ArrayList<DB_CONTENT_REC>();
        String selectQuery="SELECT * FROM "+TABLE_REC_NAME+ " WHERE "+KEY_REC_TYPE+"=\"Depot\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        //cursor based request to DB

        if (cursor.moveToFirst()){
            do{
                DB_CONTENT_REC content = new DB_CONTENT_REC();
                content.setID(Integer.parseInt(cursor.getString(0)));
                content.setFNAME(cursor.getString(1));
                content.setLNAME(cursor.getString(2));
                content.setLAT(cursor.getString(3));
                content.setLONG(cursor.getString(4));
                content.setSTRNAME(cursor.getString(5));
                content.setSTRNMBR(cursor.getString(6));
                content.setCITY(cursor.getString(7));
                content.setZIP(cursor.getString(8));
                content.setSTIME(cursor.getString(9));
                content.setETIME(cursor.getString(10));
                content.setTIMEFRAME(cursor.getString(11));
                content.setNUMBER(cursor.getInt(12));
                content.setTYPE(cursor.getString(13));
                content.setSIGNATURE(cursor.getString(14));

                contentList.add(content);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return contentList;
    }

    void updatePickup(String sscc){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(KEY_PAK_PICKUPREPORTED,"1");
        db.update(TABLE_PAK_NAME,contentValues,"sscc="+"\""+sscc+"\"",null);
        db.close();
    }

    //Called when packet gets scanned
    void updatePickupTime(String sscc, String time){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(KEY_PAK_PICKDATE,time);
        contentValues.put(KEY_PAK_SCANNED,1);
        db.update(TABLE_PAK_NAME,contentValues,"sscc="+"\""+sscc+"\"",null);
        db.close();
    }

    void updateDelivery(String sscc){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(KEY_PAK_DELIVERYREPORTED,"1");
        db.update(TABLE_PAK_NAME,contentValues,"sscc="+"\""+sscc+"\"",null);
        db.close();
    }

    void updateDeliveryTime(String sscc, String time){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(KEY_PAK_RECEIVEDATE,time);
        db.update(TABLE_PAK_NAME,contentValues,"sscc="+"\""+sscc+"\"",null);
        db.close();
    }

    void updateSignature(String rid, String sig){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(KEY_REC_SIG,sig);
        db.update(TABLE_REC_NAME,contentValues,"\""+KEY_REC_ID+"\"=\""+rid+"\"",null);
        db.close();
    }

    void updateSignatureSent(String sscc){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(KEY_PAK_SIGSENT,1);
        db.update(TABLE_PAK_NAME,contentValues,KEY_PAK_SSCC+"=\""+sscc+"\"",null);
        db.close();
    }

    public List<DB_CONTENT_REC> getNextReceiver(){
        List<DB_CONTENT_REC> contentList=new ArrayList<DB_CONTENT_REC>();
        String selectQuery="SELECT * FROM "+TABLE_REC_NAME+ " WHERE "+KEY_REC_TYPE+"=\"Receiver\" AND "+KEY_REC_SIG+"==\"0\" ORDER BY number LIMIT 1";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        //cursor based request to DB

        if (cursor.moveToFirst()){
            do{
                DB_CONTENT_REC content = new DB_CONTENT_REC();
                content.setID(Integer.parseInt(cursor.getString(0)));
                content.setFNAME(cursor.getString(1));
                content.setLNAME(cursor.getString(2));
                content.setLAT(cursor.getString(3));
                content.setLONG(cursor.getString(4));
                content.setSTRNAME(cursor.getString(5));
                content.setSTRNMBR(cursor.getString(6));
                content.setCITY(cursor.getString(7));
                content.setZIP(cursor.getString(8));
                content.setSTIME(cursor.getString(9));
                content.setETIME(cursor.getString(10));
                content.setTIMEFRAME(cursor.getString(11));
                content.setNUMBER(cursor.getInt(12));
                content.setTYPE(cursor.getString(13));

                contentList.add(content);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return contentList;
    }

    void deletePaket(String sscc){
        SQLiteDatabase db=this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_PAK_NAME+" WHERE "+KEY_PAK_SSCC+"="+sscc);
    }

    void deleteReceiver(Integer id){
        SQLiteDatabase db=this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_REC_NAME+" WHERE "+KEY_REC_ID+"="+id);
    }

    void dropAllTables(){
        SQLiteDatabase db=this.getWritableDatabase();
        db.execSQL("DROP TABLE "+TABLE_PAK_NAME);
        db.execSQL("DROP TABLE "+TABLE_REC_NAME);
    }

    public ArrayList<String> findPacketsForRID(Integer recId){
        ArrayList<String> ssccList=new ArrayList<>();
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor c=db.rawQuery("SELECT sscc FROM "+TABLE_PAK_NAME+" WHERE "+KEY_PAK_RECID+"="+recId,null);
        if (c.moveToFirst()){
            do {
                // Passing values
                String column1 = c.getString(0);
                ssccList.add(column1);
                // Do something Here with values
            } while(c.moveToNext());
        }
        c.close();
        db.close();
        return ssccList;
    }

    public String getTimeForSSCC(String sscc){
        SQLiteDatabase db=this.getReadableDatabase();
        String time="";
        Cursor c=db.rawQuery("SELECT receive_date FROM "+TABLE_PAK_NAME+" WHERE "+KEY_PAK_SSCC+"="+sscc,null);
        if (c.moveToFirst()){
            do {
                // Passing values
                time = c.getString(0);
                // Do something Here with values
            } while(c.moveToNext());
        }
        c.close();
        db.close();
        return time;
    }

    public Integer checkSentData(){
        SQLiteDatabase db=this.getReadableDatabase();
        int results=0;
        Cursor c=db.rawQuery("SELECT COUNT(*) FROM "+TABLE_PAK_NAME+" WHERE "+KEY_PAK_DELIVERYREPORTED+"=0 OR "+KEY_PAK_PICKUPREPORTED+"=0 OR "+KEY_PAK_SIGSENT+"=0",null);
        if (c.moveToFirst()){
            do {
                // Passing values
                results = c.getInt(0)+results;
                // Do something Here with values
            } while(c.moveToNext());
        }
        c.close();
        db.close();
        return results;
    }

    public Integer checkForMoreReceivers(){
        SQLiteDatabase db=this.getReadableDatabase();
        int results=0;
        Cursor c=db.rawQuery("SELECT COUNT(*) FROM "+TABLE_REC_NAME+" WHERE "+KEY_REC_SIG+"=\"0\" AND "+KEY_REC_TYPE+"=\"Receiver\"",null);
        if (c.moveToFirst()){
            do {
                // Passing values
                results = c.getInt(0);
                // Do something Here with values
            } while(c.moveToNext());
        }
        c.close();
        db.close();
        return results;
    }

    public List<DB_CONTENT_PAK> PickUpRepFail(){
        List<DB_CONTENT_PAK> contentList=new ArrayList<>();
        String selectQuery="SELECT * FROM "+TABLE_PAK_NAME+ " WHERE "+KEY_PAK_PICKUPREPORTED+"=0;";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);

        if (cursor.moveToFirst()){
            do{
                DB_CONTENT_PAK content = new DB_CONTENT_PAK();
                content.setID(Integer.parseInt(cursor.getString(0)));
                content.setSSCC(cursor.getString(1));
                content.setRECID(Integer.parseInt(cursor.getString(2)));
                content.setPICKDATE(cursor.getString(3));
                content.setRECDATE(cursor.getString(4));
                content.setPICKUP(Integer.parseInt(cursor.getString(5)));
                content.setDELIVERY(Integer.parseInt(cursor.getString(6)));
                content.setSCANNED(Integer.parseInt(cursor.getString(7)));
                content.setSIGSENT(Integer.parseInt(cursor.getString(8)));
                contentList.add(content);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return contentList;
    }

    public List<DB_CONTENT_PAK> DelRepFail(){
        List<DB_CONTENT_PAK> contentList=new ArrayList<>();
        String selectQuery="SELECT * FROM "+TABLE_PAK_NAME+ " WHERE "+KEY_PAK_DELIVERYREPORTED+"=0;";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);

        if (cursor.moveToFirst()){
            do{
                DB_CONTENT_PAK content = new DB_CONTENT_PAK();
                content.setID(Integer.parseInt(cursor.getString(0)));
                content.setSSCC(cursor.getString(1));
                content.setRECID(Integer.parseInt(cursor.getString(2)));
                content.setPICKDATE(cursor.getString(3));
                content.setRECDATE(cursor.getString(4));
                content.setPICKUP(Integer.parseInt(cursor.getString(5)));
                content.setDELIVERY(Integer.parseInt(cursor.getString(6)));
                content.setSCANNED(Integer.parseInt(cursor.getString(7)));
                content.setSIGSENT(Integer.parseInt(cursor.getString(8)));
                contentList.add(content);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return contentList;
    }

    public HashMap<String,String> SigRepFail(){
        HashMap<String,String> contentList=new HashMap<>();
        String selectQuery="SELECT PaketData.sscc, ReceiverData.signature FROM "+TABLE_PAK_NAME+ " INNER JOIN "+TABLE_REC_NAME+" ON PaketData.receiver_id=ReceiverData.id WHERE "+KEY_PAK_SIGSENT+"=0";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);

        if (cursor.moveToFirst()){
            do{
                contentList.put(cursor.getString(0),cursor.getString(1));

            } while (cursor.moveToNext());
        }
        cursor.close();
        return contentList;
    }



    public void deleteRec(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+TABLE_REC_NAME+" WHERE id="+id);
        db.execSQL("delete from "+TABLE_PAK_NAME+" WHERE receiver_id="+id);
        db.close();
    }

    public void deleteDepot(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+TABLE_REC_NAME+" WHERE stoptype=\"Depot\"");
        db.close();
    }

    /*public ArrayList<String> PickUpRepFail(){
        ArrayList<String> ssccList=new ArrayList<>();
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor c=db.rawQuery("SELECT sscc, pick_date FROM "+TABLE_PAK_NAME+" WHERE pickup_reported=0;",null);
        if (c.moveToFirst()){
            do {
                // Passing values
                ssccList.add(c.getString(0));
                ssccList.add(c.getString(1));
                // Do something Here with values
            } while(c.moveToNext());
        }
        c.close();
        db.close();
        return ssccList;
    }

    public ArrayList<String> DeliveryRepFail(){
        ArrayList<String> ssccList=new ArrayList<>();
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor c=db.rawQuery("SELECT sscc, receive_date FROM "+TABLE_PAK_NAME+" WHERE delivery_reported=0;",null);
        if (c.moveToFirst()){
            do {
                // Passing values
                ssccList.add(c.getString(0));
                ssccList.add(c.getString(1));
                // Do something Here with values
            } while(c.moveToNext());
        }
        c.close();
        db.close();
        return ssccList;
    }

    public ArrayList<String> SigRepFail(){
        ArrayList<String> sigList=new ArrayList<>();
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor c=db.rawQuery("SELECT id, signature FROM "+TABLE_REC_NAME+" WHERE signature_sent=0 AND stoptype=\"Receiver\";",null);
        if (c.moveToFirst()){
            do {
                sigList.add(c.getString(0));
                sigList.add(c.getString(1));
            } while(c.moveToNext());
        }
        c.close();
        db.close();
        return sigList;
    }*/


/*
    void addData(DBContent contact){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values=new ContentValues();
        values.put(KEY_CID,contact.getCID());

        //write values into ContentValues and then into DB
        db.insert(TABLE_NAMES,null,values);
        db.close();
    }


    void updateValue(String newval,int i){
        String attribute=KEY_NAME;
        if (i==1){attribute=KEY_LOC;}
        if (i==2){attribute=KEY_DESC;}
        if (i==3){attribute=KEY_LANG;}
        SQLiteDatabase db=this.getWritableDatabase();
        db.execSQL("UPDATE "+TABLE_NAMES+" SET "+attribute+" = "+newval+" WHERE "+attribute+" != "+newval);
    }
*/

    /*

    public void deleteall(){
        //clear Tables
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_NAMES);
        db.execSQL("delete from "+TABLE_FOLLOWING);
    }

    public void deleteenvinfo(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+TABLE_NAMES_ENV);
    }

    public int TableExists() {
        SQLiteDatabase mDatabase = this.getReadableDatabase();

        String count = "SELECT count(*) FROM CID_TWITTER";
        Cursor mcursor = mDatabase.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        mcursor.close();
        return icount;
    }

    public int TableEnvExists() {
        SQLiteDatabase mDatabase = this.getReadableDatabase();

        String count = "SELECT count(*) FROM EID";
        Cursor mcursor = mDatabase.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        mcursor.close();
        return icount;
    }*/
}
