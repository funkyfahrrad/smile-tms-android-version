package com.here.android.tutorial;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class CheckInternetCon {


    public Boolean check(Context c){
        boolean connected;
        ConnectivityManager connectivityManager = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else {
            connected = false;}
        return connected;
    }

    public AlertDialog noInternet(Context c){
        AlertDialog alertDialog=new AlertDialog.Builder(c)
                .setTitle("Internetverbindung nicht vorhanden oder stabil genug")
                .setMessage("Stelle sicher, dass du eine stabile Internetverbindung hast und probiere es erneut!")


                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }

                }).create();

        return alertDialog;

    }

}
