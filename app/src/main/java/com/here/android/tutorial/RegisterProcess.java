package com.here.android.tutorial;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.SwitchCompat;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.CustomTypeAdapter;
import com.apollographql.apollo.api.CustomTypeValue;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.google.gson.Gson;
import com.here.android.mpa.urbanmobility.Alert;
import com.here.android.tutorial.type.TimeInWeekInput;
import com.here.android.tutorial.type.WishTimeframeInput;
import com.itkacher.okhttpprofiler.OkHttpProfilerInterceptor;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Route;

import static com.here.android.tutorial.type.Weekday.FRIDAY;
import static com.here.android.tutorial.type.Weekday.MONDAY;
import static com.here.android.tutorial.type.Weekday.SATURDAY;
import static com.here.android.tutorial.type.Weekday.THURSDAY;
import static com.here.android.tutorial.type.Weekday.TUESDAY;
import static com.here.android.tutorial.type.Weekday.WEDNESDAY;

public class RegisterProcess extends AppCompatActivity {

    Spinner vehicletype;
    Button sendData;
    LinearLayout pData_lo, vData_lo, rData_lo, tData_lo;
    int height, height1;
    LinearLayout pdataTitle_lo, vdataTitle_lo, rdataTitle_lo, adataTitle_lo;
    EditText e_pdataname,e_pdatafname,e_pdatafirm,e_pdatastreet,e_pdatazip,e_pdataplace;
    Boolean b_pdataname,b_pdatafname,b_pdatafirm,b_pdatastreet,b_pdatazip,b_pdataplace, p_correct;
    EditText e_vdatavolume,e_vdataweight,e_vdatamaxleng,e_vdataheight,e_vdatawidth;
    Boolean b_vdatavolume,b_vdataweight,b_vdatamaxleng,b_vdataheight,b_vdatawidth, v_correct;
    EditText e_rregion,e_rmaxdistance,e_rforeruntime, e_rbaseprice,e_rpricepkm, e_rpricepstop;
    Boolean b_rregion,b_rmaxdistance,b_rforeruntime, b_rbaseprice,b_rpricepkm, b_rpricepstop, r_correct;
    EditText e_amofrom,e_amoto,e_atufrom,e_atuto,e_awefrom,e_aweto,e_athfrom,e_athto,e_afrfrom,e_afrto,e_asafrom,e_asato,e_asufrom,e_asuto;
    Boolean b_amo, b_atu, b_awe,b_ath, b_afr, b_asa, b_asu, b_correct;
    SwitchCompat sw_mo,sw_tu,sw_we,sw_th,sw_fr,sw_sa,sw_su;
    TextView tv_amo,tv_atu, tv_awe,tv_ath, tv_afr, tv_asa, tv_asu;
    ApolloClient apolloClient;
    private static final String TAG = "RegisterProcess";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registerprocessdata);

        sendData=findViewById(R.id.btn_send);
        apolloClient=ApolloClient.builder()
                .serverUrl("https://smile.goodstag.com/graphql")
                .okHttpClient(
                        new OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS)
                                .writeTimeout(30, TimeUnit.SECONDS)
                                .readTimeout(30, TimeUnit.SECONDS)
                                .addInterceptor(new OkHttpProfilerInterceptor())
                                .authenticator(new Authenticator() {
                                    @androidx.annotation.Nullable
                                    @Override
                                    public Request authenticate(@androidx.annotation.Nullable Route route, okhttp3.Response response) throws IOException {
                                        String credentials= Credentials.basic("smile","sei7ieQuueka");
                                        return response.request().newBuilder().header("Authorization",credentials).build();
                                    }
                                })
                                .build()

                ).build();

        final String stringFromPrefs = PreferenceManager.getDefaultSharedPreferences(this).getString("CreateDelMutation", "nothing");
        assert stringFromPrefs != null : "CreateDelMutation is null";
        if (!stringFromPrefs.equals("nothing")&&stringFromPrefs!=null){
            new AlertDialog.Builder(RegisterProcess.this).setTitle("")
                    .setMessage("Möchtest du dein bereits erstelltes Profil absenden oder ein neues erstellen?")
                    .setPositiveButton("Absenden", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Gson gson=new Gson();
                            CreateDelMutation createDelMutation=gson.fromJson(stringFromPrefs,CreateDelMutation.class);
                            sendDataToServer(createDelMutation);
                        }
                    })
                    .setPositiveButton("Neues Profil", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            SharedPreferences spreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor spreferencesEditor = spreferences.edit();
                            spreferencesEditor.remove("CreateDelMutation"); //we are removing prodId by key
                            spreferencesEditor.apply();
                            RegisterProcess.this.recreate();
                        }
                    })
                    .show();
        }
        else{
            p_correct=v_correct=r_correct=b_correct=false;
            pData_lo=findViewById(R.id.pData_lo);
            vData_lo=findViewById(R.id.vData_lo);
            rData_lo=findViewById(R.id.rData_lo);
            tData_lo=findViewById(R.id.tData_lo);

            pdataTitle_lo=findViewById(R.id.pdatatitle_lo);
            vdataTitle_lo=findViewById(R.id.vdatatitle_lo);
            rdataTitle_lo=findViewById(R.id.rdatatitle_lo);
            adataTitle_lo=findViewById(R.id.adatatitle_lo);


            //Personal data set Edits and add TextWatchers

            e_pdataname=findViewById(R.id.pinfoname);
            e_pdataname.addTextChangedListener(new TWatcher(e_pdataname));

            e_pdatafname=findViewById(R.id.pinfofname);
            e_pdatafname.addTextChangedListener(new TWatcher(e_pdatafname));

            e_pdatafirm=findViewById(R.id.pinfofirma);
            e_pdatafirm.addTextChangedListener(new TWatcher(e_pdatafirm));

            e_pdatastreet=findViewById(R.id.pinfostreet);
            e_pdatastreet.addTextChangedListener(new TWatcher(e_pdatastreet));

            e_pdatazip=findViewById(R.id.pinfozip);
            e_pdatazip.addTextChangedListener(new TWatcher(e_pdatazip));

            e_pdataplace=findViewById(R.id.pinfoplace);
            e_pdataplace.addTextChangedListener(new TWatcher(e_pdataplace));


            //Vehicle Info set Edits and add TextWatchers

            e_vdatavolume=findViewById(R.id.vladeedit);
            e_vdatavolume.addTextChangedListener(new TWatcher(e_vdatavolume));

            e_vdataweight=findViewById(R.id.vzuladeedit);
            e_vdataweight.addTextChangedListener(new TWatcher(e_vdataweight));

            e_vdatamaxleng=findViewById(R.id.vlaengedit);
            e_vdatamaxleng.addTextChangedListener(new TWatcher(e_vdatamaxleng));

            e_vdataheight=findViewById(R.id.vhoeheedit);
            e_vdataheight.addTextChangedListener(new TWatcher(e_vdataheight));

            e_vdatawidth=findViewById(R.id.vbreiteedit);
            e_vdatawidth.addTextChangedListener(new TWatcher(e_vdatawidth));


            //Regions set Edits and add TextWatchers

            e_rregion=findViewById(R.id.region_edit);
            e_rregion.addTextChangedListener(new TWatcher(e_rregion));

            e_rmaxdistance=findViewById(R.id.maxdist_edit);
            e_rmaxdistance.addTextChangedListener(new TWatcher(e_rmaxdistance));

            e_rforeruntime=findViewById(R.id.forerun_edit);
            e_rforeruntime.addTextChangedListener(new TWatcher(e_rforeruntime));

            e_rbaseprice=findViewById(R.id.baseprice_edit);
            e_rbaseprice.addTextChangedListener(new TWatcher(e_rbaseprice));

            e_rpricepkm=findViewById(R.id.priceperkm_edit);
            e_rpricepkm.addTextChangedListener(new TWatcher(e_rpricepkm));

            e_rpricepstop=findViewById(R.id.priceperstop_edit);
            e_rpricepstop.addTextChangedListener(new TWatcher(e_rpricepstop));

            //Availablities set Edits, spinners and add TextWatchers

            e_amofrom=findViewById(R.id.amofrom_edit);
            e_amofrom.addTextChangedListener(new TWatcher(e_amofrom));

            e_amoto=findViewById(R.id.amoto_edit);
            e_amoto.addTextChangedListener(new TWatcher(e_amoto));

            e_atufrom=findViewById(R.id.atufrom_edit);
            e_atufrom.addTextChangedListener(new TWatcher(e_atufrom));

            e_atuto=findViewById(R.id.atuto_edit);
            e_atuto.addTextChangedListener(new TWatcher(e_atuto));

            e_awefrom=findViewById(R.id.awefrom_edit);
            e_awefrom.addTextChangedListener(new TWatcher(e_awefrom));

            e_aweto=findViewById(R.id.aweto_edit);
            e_aweto.addTextChangedListener(new TWatcher(e_aweto));

            e_athfrom=findViewById(R.id.athfrom_edit);
            e_athfrom.addTextChangedListener(new TWatcher(e_athfrom));

            e_athto=findViewById(R.id.athto_edit);
            e_athto.addTextChangedListener(new TWatcher(e_athto));

            e_afrfrom=findViewById(R.id.afrfrom_edit);
            e_afrfrom.addTextChangedListener(new TWatcher(e_afrfrom));

            e_afrto=findViewById(R.id.afrto_edit);
            e_afrto.addTextChangedListener(new TWatcher(e_afrto));

            e_asafrom=findViewById(R.id.asafrom_edit);
            e_asafrom.addTextChangedListener(new TWatcher(e_asafrom));

            e_asato=findViewById(R.id.asato_edit);
            e_asato.addTextChangedListener(new TWatcher(e_asato));

            e_asufrom=findViewById(R.id.asufrom_edit);
            e_asufrom.addTextChangedListener(new TWatcher(e_asufrom));

            e_asuto=findViewById(R.id.asuto_edit);
            e_asuto.addTextChangedListener(new TWatcher(e_asuto));

            tv_amo=findViewById(R.id.tv_amo);
            tv_atu=findViewById(R.id.tv_atu);
            tv_awe=findViewById(R.id.tv_awed);
            tv_ath=findViewById(R.id.tv_ath);
            tv_afr=findViewById(R.id.tv_afr);
            tv_asa=findViewById(R.id.tv_asat);
            tv_asu=findViewById(R.id.tv_asun);
            sw_mo=findViewById(R.id.s_mo);
            sw_tu=findViewById(R.id.s_tu);
            sw_we=findViewById(R.id.s_we);
            sw_th=findViewById(R.id.s_th);
            sw_fr=findViewById(R.id.s_fr);
            sw_sa=findViewById(R.id.s_sat);
            sw_su=findViewById(R.id.s_sun);

            vehicletype=findViewById(R.id.spinnervehicle);


            String[] items=new String[]{"Fahrzeugart wählen","Fahrrad","Lastenrad","PKW","Kastenkombi","Transporter"};
            ArrayAdapter<String> adapter=new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,items);
            vehicletype.setAdapter(adapter);
            vehicletype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if(i==0){
                        e_vdatavolume.setText("");
                        e_vdataweight.setText("");
                        e_vdatamaxleng.setText("");
                        e_vdataheight.setText("");
                        e_vdatawidth.setText("");
                    }
                    if (i==1){
                        e_vdatavolume.setText("5");
                        e_vdataweight.setText("5");
                        e_vdatamaxleng.setText("38");
                        e_vdataheight.setText("35");
                        e_vdatawidth.setText("20");
                    }
                    if (i==2){
                        e_vdatavolume.setText("10");
                        e_vdataweight.setText("100");
                        e_vdatamaxleng.setText("63");
                        e_vdataheight.setText("50");
                        e_vdatawidth.setText("35");
                    }
                    if (i==3){
                        e_vdatavolume.setText("20");
                        e_vdataweight.setText("500");
                        e_vdatamaxleng.setText("102");
                        e_vdataheight.setText("64");
                        e_vdatawidth.setText("77");
                    }
                    if (i==4){
                        e_vdatavolume.setText("40");
                        e_vdataweight.setText("800");
                        e_vdatamaxleng.setText("200");
                        e_vdataheight.setText("120");
                        e_vdatawidth.setText("170");
                    }
                    if (i==5){
                        e_vdatavolume.setText("80");
                        e_vdataweight.setText("1500");
                        e_vdatamaxleng.setText("350");
                        e_vdataheight.setText("200");
                        e_vdatawidth.setText("150");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            //Ovservers for collapsing/expanding LinearLayouts

            pData_lo.getViewTreeObserver().addOnPreDrawListener(
                    new ViewTreeObserver.OnPreDrawListener() {

                        @Override
                        public boolean onPreDraw() {
                            pData_lo.getViewTreeObserver().removeOnPreDrawListener(this);
                            final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                            final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                            pData_lo.measure(widthSpec, heightSpec);
                            height = pData_lo.getMeasuredHeight();
                            return true;
                        }
                    });

            vData_lo.getViewTreeObserver().addOnPreDrawListener(
                    new ViewTreeObserver.OnPreDrawListener() {

                        @Override
                        public boolean onPreDraw() {
                            vData_lo.getViewTreeObserver().removeOnPreDrawListener(this);
                            final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                            final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                            vData_lo.measure(widthSpec, heightSpec);
                            height = vData_lo.getMeasuredHeight();
                            return true;
                        }
                    });
            rData_lo.getViewTreeObserver().addOnPreDrawListener(
                    new ViewTreeObserver.OnPreDrawListener() {

                        @Override
                        public boolean onPreDraw() {
                            rData_lo.getViewTreeObserver().removeOnPreDrawListener(this);
                            final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                            final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                            rData_lo.measure(widthSpec, heightSpec);
                            height = rData_lo.getMeasuredHeight();
                            return true;
                        }
                    });
            tData_lo.getViewTreeObserver().addOnPreDrawListener(
                    new ViewTreeObserver.OnPreDrawListener() {

                        @Override
                        public boolean onPreDraw() {
                            tData_lo.getViewTreeObserver().removeOnPreDrawListener(this);
                            final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                            final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                            tData_lo.measure(widthSpec, heightSpec);
                            height = tData_lo.getMeasuredHeight();
                            return true;
                        }
                    });
            pData_lo.setVisibility(View.GONE);
            vData_lo.setVisibility(View.GONE);
            rData_lo.setVisibility(View.GONE);
            tData_lo.setVisibility(View.GONE);

            //initially set all Booleans (contains text) false
            b_pdataname=b_pdatafname=b_pdatafirm=b_pdatastreet=b_pdatazip=b_pdataplace=false;

            b_vdatavolume=b_vdataweight=b_vdatamaxleng=b_vdataheight=b_vdatawidth=false;

            b_rregion=b_rmaxdistance=b_rforeruntime=b_rbaseprice=b_rpricepkm=b_rpricepstop=false;

            b_amo= b_atu= b_awe=b_ath= b_afr= b_asa= b_asu=false;


        }


       /*
       * lade=findViewById(R.id.vladeedit);
        zulade=findViewById(R.id.vzuladeedit);
        laenge=findViewById(R.id.vlaengedit);
        hoehe=findViewById(R.id.vhoeheedit);
        breite=findViewById(R.id.vbreiteedit);
       *
       * */
        //EditText e_vdatavolume,e_vdataweight,e_vdatamaxleng,e_vdataheight,e_vdatawidth;
        //EditText e_zustellgeb,e_maxdistance,e_foreruntime, e_baseprice,e_pricepkm, e_pricepstop;
        //EditText e_movon,e_mobis,e_divon,e_dibis,e_mivon,e_mibis,e_dovon,e_dobis,e_frvon,e_frbis,e_savon,e_sabis,e_sovon,e_sobis;

    }



    public void getTime(View v){
        final EditText eReminderTime=findViewById(v.getId());
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(RegisterProcess.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String min="";
                String h="";
                if (selectedMinute<10){
                    min="0"+selectedMinute;
                }
                else{
                    min=String.valueOf(selectedMinute);
                }
                if(selectedHour<10){
                    h="0"+selectedHour;
                }
                else{
                    h=String.valueOf(selectedHour);
                }
                eReminderTime.setText(h+":"+min);
                CheckTimeCorr(eReminderTime);

            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private void CheckTimeCorr(EditText e){

        b_correct=false;
        if (((e==e_amofrom)||(e==e_amoto))&&!e_amofrom.getText().toString().equals("")&&!e_amoto.getText().toString().equals("")){
            String fromTime=e_amofrom.getText().toString().replace(":","");
            String toTime=e_amoto.getText().toString().replace(":","");
            if (Integer.parseInt(fromTime)>=Integer.parseInt(toTime)){
                e.setText("");
                alertWrongTimeFormat("Montag");
                b_amo=false;
                b_correct=false;
            }
            else{
                b_amo=true;
                b_correct=true;
            }
        }
        if (((e==e_atufrom)||(e==e_atuto))&&!e_atufrom.getText().toString().equals("")&&!e_atuto.getText().toString().equals("")){
            String fromTime=e_atufrom.getText().toString().replace(":","");
            String toTime=e_atuto.getText().toString().replace(":","");
            if (Integer.parseInt(fromTime)>=Integer.parseInt(toTime)){
                e.setText("");
                alertWrongTimeFormat("Dienstag");
                b_atu=false;
                b_correct=false;
            }
            else{
                b_atu=true;
                b_correct=true;
            }
        }
        if (((e==e_awefrom)||(e==e_aweto))&&!e_awefrom.getText().toString().equals("")&&!e_aweto.getText().toString().equals("")){
            String fromTime=e_awefrom.getText().toString().replace(":","");
            String toTime=e_aweto.getText().toString().replace(":","");
            if (Integer.parseInt(fromTime)>=Integer.parseInt(toTime)){
                e.setText("");
                alertWrongTimeFormat("Mittwoch");
                b_awe=false;
                b_correct=false;
            }
            else{
                b_awe=true;
                b_correct=true;
            }
        }
        if (((e==e_athfrom)||(e==e_athto))&&!e_athfrom.getText().toString().equals("")&&!e_athto.getText().toString().equals("")){
            String fromTime=e_athfrom.getText().toString().replace(":","");
            String toTime=e_athto.getText().toString().replace(":","");
            if (Integer.parseInt(fromTime)>=Integer.parseInt(toTime)){
                e.setText("");
                alertWrongTimeFormat("Donnerstag");
                b_ath=false;
                b_correct=false;
            }
            else{
                b_ath=true;
                b_correct=true;
            }
        }
        if (((e==e_afrfrom)||(e==e_afrto))&&!e_afrfrom.getText().toString().equals("")&&!e_afrto.getText().toString().equals("")){
            String fromTime=e_afrfrom.getText().toString().replace(":","");
            String toTime=e_afrto.getText().toString().replace(":","");
            if (Integer.parseInt(fromTime)>=Integer.parseInt(toTime)){
                e.setText("");
                alertWrongTimeFormat("Freitag");
                b_afr=false;
                b_correct=false;
            }
            else{
                b_afr=true;
                b_correct=true;
            }
        }
        if (((e==e_asafrom)||(e==e_asato))&&!e_asafrom.getText().toString().equals("")&&!e_asato.getText().toString().equals("")){
            String fromTime=e_asafrom.getText().toString().replace(":","");
            String toTime=e_asato.getText().toString().replace(":","");
            if (Integer.parseInt(fromTime)>=Integer.parseInt(toTime)){
                e.setText("");
                alertWrongTimeFormat("Samstag");
                b_asa=false;
                b_correct=false;
            }
            else{
                b_asa=true;
                b_correct=true;
            }
        }
        if (((e==e_asufrom)||(e==e_asuto))&&!e_asufrom.getText().toString().equals("")&&!e_asuto.getText().toString().equals("")){
            String fromTime=e_asufrom.getText().toString().replace(":","");
            String toTime=e_asuto.getText().toString().replace(":","");
            if (Integer.parseInt(fromTime)>=Integer.parseInt(toTime)){
                e.setText("");
                alertWrongTimeFormat("Sonntag");
                b_asu=false;
                b_correct=false;
            }
            else{
                b_asu=true;
                b_correct=true;
            }
        }
        if (b_correct){
            adataTitle_lo.setBackgroundColor(getResources().getColor(R.color.colorTSM));
            allCorrect();
        }
        else{
            adataTitle_lo.setBackgroundColor(getResources().getColor(R.color.lightred));
        }
    }



    private void alertWrongTimeFormat(String day){
        new AlertDialog.Builder(RegisterProcess.this)
                .setTitle("Zeit falsch angegeben!")
                .setMessage("Stelle sicher dass du die Zeit richtig eingestellt hast am "+day+"!")


                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        
                    }

                })

                .show();
    }


    private void expand(LinearLayout layout, int layoutHeight) {
        layout.setVisibility(View.VISIBLE);
        ValueAnimator animator = slideAnimator(layout, 0, layoutHeight);
        animator.start();
    }

    private void collapse(final LinearLayout layout) {
        int finalHeight = layout.getHeight();
        ValueAnimator mAnimator = slideAnimator(layout, finalHeight, 0);

        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animator) {
                //Height=0, but it set visibility to GONE
                layout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        mAnimator.start();
    }

    private ValueAnimator slideAnimator(final LinearLayout layout, int start, int end) {
        ValueAnimator animator = ValueAnimator.ofInt(start, end);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();

                ViewGroup.LayoutParams layoutParams = layout.getLayoutParams();
                layoutParams.height = value;
                layout.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }


    public void togglePData(View view) {
        if (pData_lo.getVisibility() == View.GONE) {
            expand(pData_lo, height);
        } else {
            collapse(pData_lo);
        }
    }

    public void toggleVData(View view) {
        if (vData_lo.getVisibility() == View.GONE) {
            expand(vData_lo, height);
        } else {
            collapse(vData_lo);
        }
    }

    public void toggleRData(View view){
        if (rData_lo.getVisibility() == View.GONE) {
            expand(rData_lo, height);
        } else {
            collapse(rData_lo);
        }
    }

    public void toggleTData(View view){
        if (tData_lo.getVisibility() == View.GONE) {
            expand(tData_lo, height);
        } else {
            collapse(tData_lo);
        }
    }

    private class TWatcher implements TextWatcher {

        private View view;
        private TWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        public void afterTextChanged(Editable editable) {

            int idEditable=view.getId();
            boolean notempty=false;
            String content=editable.toString();
            if(editable.toString().length()>0){
                notempty=true;
            }

            if(e_pdataname.getId()==idEditable){
                b_pdataname=notempty;
            }
            if(e_pdatafname.getId()==idEditable){
                b_pdatafname=notempty;
            }
            if(e_pdatafirm.getId()==idEditable){
                b_pdatafirm=notempty;
            }
            if(e_pdatastreet.getId()==idEditable){
                b_pdatastreet=notempty;
            }
            if(e_pdataplace.getId()==idEditable){
                b_pdataplace=notempty;
            }
            if(e_pdatazip.getId()==idEditable){
                b_pdatazip=notempty;
            }

            if(e_vdatavolume.getId()==idEditable){
                b_vdatavolume=notempty;
            }
            if(e_vdatamaxleng.getId()==idEditable){
                b_vdatamaxleng=notempty;
            }
            if(e_vdataweight.getId()==idEditable){
                b_vdataweight=notempty;
            }
            if(e_vdataheight.getId()==idEditable){
                b_vdataheight=notempty;
            }
            if(e_vdatawidth.getId()==idEditable){
                b_vdatawidth=notempty;
            }

            if(e_rregion.getId()==idEditable){
                b_rregion=notempty;
            }
            if(e_rmaxdistance.getId()==idEditable){
                b_rmaxdistance=notempty;
            }
            if(e_rforeruntime.getId()==idEditable){
                b_rforeruntime=notempty;
            }
            if(e_rbaseprice.getId()==idEditable){
                b_rbaseprice=notempty;
            }
            if(e_rpricepkm.getId()==idEditable){
                b_rpricepkm=notempty;
            }
            if(e_rpricepstop.getId()==idEditable){
                b_rpricepstop=notempty;
            }


            if(b_pdatazip&&b_pdataplace&&b_pdatastreet&&b_pdataname&&b_pdatafname&&b_pdatafirm){
                pdataTitle_lo.setBackgroundColor(getResources().getColor(R.color.colorTSM));
                p_correct=true;
                allCorrect();
            }
            else
            {
                pdataTitle_lo.setBackgroundColor(getResources().getColor(R.color.lightred));
                p_correct=false;
            }


            if(b_vdataheight&&b_vdatawidth&&b_vdatavolume&&b_vdatamaxleng&&b_vdataweight){
                vdataTitle_lo.setBackgroundColor(getResources().getColor(R.color.colorTSM));
                v_correct=true;
                allCorrect();
            }
            else{
                vdataTitle_lo.setBackgroundColor(getResources().getColor(R.color.lightred));
                v_correct=false;
            }

            if(b_rregion&&b_rbaseprice&&b_rforeruntime&&b_rmaxdistance&&b_rpricepkm&&b_rpricepstop){
                rdataTitle_lo.setBackgroundColor(getResources().getColor(R.color.colorTSM));
                r_correct=true;
                allCorrect();
            }
            else{
                r_correct=false;
                rdataTitle_lo.setBackgroundColor(getResources().getColor(R.color.lightred));
            }
        }
    }

    public void onSwitchClicked(View v) {
        // TODO Auto-generated method stub
        boolean checked=false;
        int colorFrom= getResources().getColor(R.color.colorTSM);
        int colorTo = getResources().getColor(R.color.colorTSM);
        int switchId=v.getId();
        boolean switchIdBool=((SwitchCompat) v).isChecked();
        if(switchIdBool){
            checked=true;
            colorFrom = getResources().getColor(R.color.lightred);
        }
        else{
            colorTo= getResources().getColor(R.color.lightred);
        }
        //TransitionDrawable transition=(TransitionDrawable) v.getBackground();
        View tv_view=v;
        if (switchId==sw_mo.getId()){
            e_amofrom.setEnabled(checked);
            e_amoto.setEnabled(checked);
            b_amo=checked;
            tv_view=tv_amo;
            if (checked){CheckTimeCorr(e_amofrom);}
        }
        if (switchId==sw_tu.getId()){
            e_atufrom.setEnabled(checked);
            e_atuto.setEnabled(checked);
            b_atu=checked;
            tv_view=tv_atu;
            if (checked){CheckTimeCorr(e_atufrom);}
        }
        if (switchId==sw_we.getId()){
            e_awefrom.setEnabled(checked);
            e_aweto.setEnabled(checked);
            b_awe=checked;
            tv_view=tv_awe;
            if (checked){CheckTimeCorr(e_awefrom);}
        }
        if (switchId==sw_th.getId()){
            e_athfrom.setEnabled(checked);
            e_athto.setEnabled(checked);
            b_ath=checked;
            tv_view=tv_ath;
            if (checked){CheckTimeCorr(e_athfrom);}
        }
        if (switchId==sw_fr.getId()){
            e_afrfrom.setEnabled(checked);
            e_afrto.setEnabled(checked);
            b_afr=checked;
            tv_view=tv_afr;
            if(checked){CheckTimeCorr(e_afrfrom);}
        }
        if (switchId==sw_sa.getId()){
            e_asafrom.setEnabled(checked);
            e_asato.setEnabled(checked);
            b_asa=checked;
            tv_view=tv_asa;
            if (checked){CheckTimeCorr(e_asafrom);}
        }
        if (switchId==sw_su.getId()){
            e_asufrom.setEnabled(checked);
            e_asuto.setEnabled(checked);
            b_asu=checked;
            tv_view=tv_asu;
            if (checked){CheckTimeCorr(e_asufrom);}
        }

        final View view=tv_view;
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.setDuration(250); // milliseconds
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                view.setBackgroundColor((int) animator.getAnimatedValue());
            }

        });
        colorAnimation.start();
        if(!sw_mo.isChecked()&&!sw_tu.isChecked()&&!sw_we.isChecked()&&!sw_th.isChecked()&&!sw_fr.isChecked()&&!sw_sa.isChecked()&&!sw_su.isChecked()){
            b_correct=false;
            adataTitle_lo.setBackgroundColor(getResources().getColor(R.color.lightred));
        }

        //here you can do anythig for your particular Switch onCheckedChanged..

    }

    private void allCorrect(){
        if(b_correct&&p_correct&&r_correct&&v_correct){
            sendData.setEnabled(true);
        }
    }


    public void sendData(View v){
        CustomTypeAdapter<JSONArray> JSONArrayAdapter=new CustomTypeAdapter<JSONArray>() {
            @Override
            public JSONArray decode(@NotNull CustomTypeValue value) {
                return null;
            }

            @NotNull
            @Override
            public CustomTypeValue encode(@NotNull JSONArray value) {
                return CustomTypeValue.fromRawValue(value);
            }
        };
        int costsPerH=Integer.parseInt(e_rbaseprice.getText().toString());
        int costsPerKm=Integer.parseInt(e_rpricepkm.getText().toString());
        int costsPerStop=Integer.parseInt(e_rpricepstop.getText().toString());
        ArrayList<TimeInWeekInput> cutOffTimes=cutoff();
        String firstname=e_pdatafname.getText().toString();
        String lastname=e_pdataname.getText().toString();
        List<String> deliveryAreas=new ArrayList<>();
        deliveryAreas.add(e_rregion.getText().toString());//TODO Aufteilen!
        Float delCapacatiyVol=0.0f;
        Float  delCapacityWeight=Float.parseFloat(e_vdataweight.getText().toString());
        int maxPackets=Integer.parseInt(e_vdatavolume.getText().toString());
        int maxStopsPTour=100;  //TODO ???
        String tmsCarrieId="0"; //PLACEHOLDER
        String vehicType="";
        if (!vehicletype.getSelectedItem().toString().equals("Fahrzeugart wählen")){
            vehicType=vehicletype.getSelectedItem().toString();
        }
        else{
            vehicType="customVehicleInput";
        }

        List<WishTimeframeInput> wTimes=Timeframes();
        //WishTimeframeInput [] wishtimes= new WishTimeframeInput[];
        //JSONArray wishTimeFrames=Timeframes();

        CreateDelMutation delMutation= CreateDelMutation.builder().costsPerHour(costsPerH).costsPerKm(costsPerKm).costsPerStop(costsPerStop).cufOffTimes(cutOffTimes)
                .delivererFirstName(firstname).delivererLastName(lastname).deliveryAreas(deliveryAreas).deliveryCapacityVolume(delCapacatiyVol).deliveryCapacityWeight(delCapacityWeight)
        .maxPacketnumberPerTour(maxPackets).maxStopsPerTour(maxStopsPTour).tmsCarrierId(tmsCarrieId).vehicleType(vehicType).wishTimeframes(wTimes).build();
        SharedPreferences mPrefs = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor prefEditor=mPrefs.edit();
        Gson gson=new Gson();
        String json=gson.toJson(delMutation);
        prefEditor.putString("CreateDelMutation", json);
        prefEditor.apply();
        sendDataToServer(delMutation);

    }

    private void sendDataToServer(CreateDelMutation delMutation){
        CheckInternetCon checkInternetCon=new CheckInternetCon();
        boolean check=checkInternetCon.check(RegisterProcess.this);
        if (check){
            apolloClient
                    .mutate(delMutation)
                    .enqueue(
                            new ApolloCall.Callback<CreateDelMutation.Data>() {
                                @Override
                                public void onResponse(@NotNull Response<CreateDelMutation.Data> response) {
                                    Log.i(TAG, response.toString());
                                    //"AlreadyCompleted" for later use, so profile can be edited (no need to create a new one via GraphQL)
                                    PreferenceManager.getDefaultSharedPreferences(RegisterProcess.this).edit().putBoolean("AlreadyCompleted",true).apply();
                                    new AlertDialog.Builder(RegisterProcess.this).setTitle("Profil erfolgreich vervollständigt")
                                            .setMessage("Du kannst dich jetzt anmelden und deine Touren abfragen!")
                                            .setPositiveButton("Weiter", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    finish();
                                                }
                                            })
                                            .show();
                                }

                                @Override
                                public void onFailure(@NotNull ApolloException e) {
                                    Log.e(TAG, e.getMessage(), e);
                                    new AlertDialog.Builder(RegisterProcess.this).setTitle("Ein Fehler ist aufgetreten")
                                            .setMessage("Bitte probiere es noch einmal oder wende dich an den Kundensupport wenn das Problem bestehen bleibt")
                                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {

                                                }
                                            })
                                            .show();

                                }
                            }
                    );
        }
        else{
            AlertDialog alertDialog=checkInternetCon.noInternet(RegisterProcess.this);
            alertDialog.show();
        }
    }


    private ArrayList<TimeInWeekInput> cutoff(){
        ArrayList<TimeInWeekInput> cutoff=new ArrayList<>();
        //TODO
        TimeInWeekInput tiW=TimeInWeekInput.builder().day(MONDAY).hour(12).minute(0).build();
        cutoff.add(tiW);


        return cutoff;
    }

    private ArrayList<WishTimeframeInput> Timeframes(){
        JSONArray timef=new JSONArray();
        ArrayList<WishTimeframeInput> wti= new ArrayList<>();
        if (sw_mo.isChecked()) {
                int start=Integer.parseInt(e_amofrom.getText().toString().replace(":", ""));
                int end=Integer.parseInt(e_amoto.getText().toString().replace(":", ""));
                WishTimeframeInput wt= WishTimeframeInput.builder().day(MONDAY).startTime(start).endTime(end).build();
                wti.add(wt);
        }
        if (sw_tu.isChecked()) {
            int start=Integer.parseInt(e_atufrom.getText().toString().replace(":", ""));
            int end=Integer.parseInt(e_atuto.getText().toString().replace(":", ""));
            WishTimeframeInput wt= WishTimeframeInput.builder().day(TUESDAY).startTime(start).endTime(end).build();
            wti.add(wt);
        }
        if (sw_we.isChecked()) {
            int start=Integer.parseInt(e_awefrom.getText().toString().replace(":", ""));
            int end=Integer.parseInt(e_aweto.getText().toString().replace(":", ""));
            WishTimeframeInput wt= WishTimeframeInput.builder().day(WEDNESDAY).startTime(start).endTime(end).build();
            wti.add(wt);
        }
        if (sw_th.isChecked()) {
            int start=Integer.parseInt(e_athfrom.getText().toString().replace(":", ""));
            int end=Integer.parseInt(e_athto.getText().toString().replace(":", ""));
            WishTimeframeInput wt= WishTimeframeInput.builder().day(THURSDAY).startTime(start).endTime(end).build();
            wti.add(wt);
        }
        if (sw_fr.isChecked()) {
            int start=Integer.parseInt(e_afrfrom.getText().toString().replace(":", ""));
            int end=Integer.parseInt(e_afrto.getText().toString().replace(":", ""));
            WishTimeframeInput wt= WishTimeframeInput.builder().day(FRIDAY).startTime(start).endTime(end).build();
            wti.add(wt);
        }
        if (sw_sa.isChecked()) {
            int start=Integer.parseInt(e_asafrom.getText().toString().replace(":", ""));
            int end=Integer.parseInt(e_asato.getText().toString().replace(":", ""));
            WishTimeframeInput wt= WishTimeframeInput.builder().day(SATURDAY).startTime(start).endTime(end).build();
            wti.add(wt);
        }
        return wti;
    }

}


