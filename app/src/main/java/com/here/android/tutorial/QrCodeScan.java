package com.here.android.tutorial;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Toast;

public class QrCodeScan extends AppCompatActivity {

    String tourinfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qrcodescanlayout);
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("LastActivity","QrCodeScan").apply();
        Intent getint=getIntent();
        tourinfo=getint.getStringExtra("tourinfos");
    }

    public void qrproceed(View v){
        Toast.makeText(QrCodeScan.this,"Authentifizierung erfolgreich",Toast.LENGTH_LONG).show();
        //Intent intent=new Intent(QrCodeScan.this,TourSteps.class);
        Intent intent=new Intent(QrCodeScan.this,PickupPackets.class);
        //intent.putExtra("step",0);
        intent.putExtra("tourinfos",tourinfo);
        startActivity(intent);
        finish();
    }
}
