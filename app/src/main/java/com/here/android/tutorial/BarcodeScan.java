package com.here.android.tutorial;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.journeyapps.barcodescanner.DefaultDecoderFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class BarcodeScan extends Activity {

    private DecoratedBarcodeView barcodeView;
    private BeepManager beepManager;
    String lastText;
    Integer check;
    private ArrayList<String> ssccList=new ArrayList<String>();
    private ArrayList<String> scannedssccs=new ArrayList<String>();

    private ArrayAdapter<String> adapter;

    private BarcodeCallback callback=new BarcodeCallback(){
        @Override
        public void barcodeResult(BarcodeResult result){
            if(result.getText() == null || result.getText().equals(lastText)) {
                // Prevent duplicate scans
                return;
            }
            check=0;
            lastText = result.getText();
            //tv.setText(lastText);
            if (scannedssccs.contains(lastText)){
                Toast.makeText(BarcodeScan.this,"Paket wurde bereits registriert",Toast.LENGTH_LONG).show();
                check=1;
            }
            if (ssccList.contains(lastText)){
                SimpleDateFormat currentDay=new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                SimpleDateFormat currentTime=new SimpleDateFormat("HH:mm:ss.000Z",Locale.getDefault());
                String cTime=currentDay.format(new Date())+"T"+currentTime.format(new Date());
                final DatabaseHandler db=new DatabaseHandler(BarcodeScan.this);
                db.updateDeliveryTime(lastText, cTime);
                //db.updateDelivery(lastText);
                Toast.makeText(BarcodeScan.this,"Paket wurde erfolgreich registriert",Toast.LENGTH_LONG).show();
                scannedssccs.add(lastText);
                ssccList.remove(lastText);
                adapter.notifyDataSetChanged();
                check=1;
            }
            if(check==0){
                Toast.makeText(BarcodeScan.this,"Paket nicht in Tour enthalten",Toast.LENGTH_LONG).show();
            }
            if (ssccList.size()==0){
                new AlertDialog.Builder(BarcodeScan.this)
                        .setTitle("Alle Pakete wurden erfolgreich eingescannt!")
                        .setMessage("Du kannst die Pakete jetzt übergeben!")

                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }

                        })

                        .show();
            }
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints){

        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pickuppaks_lo);
        ListView ssccLV=findViewById(R.id.ssccListView);
        Intent getIntent=getIntent();
        ssccList=getIntent.getStringArrayListExtra("packetList");
        adapter=new ArrayAdapter<String>(BarcodeScan.this, R.layout.sscclistscan,ssccList);
        ssccLV.setAdapter(adapter);
        barcodeView=findViewById(R.id.barcode_scanner);
        Collection<BarcodeFormat> formats= Arrays.asList(BarcodeFormat.QR_CODE,BarcodeFormat.CODE_39);
        barcodeView.getBarcodeView().setDecoderFactory(new DefaultDecoderFactory(formats));
        barcodeView.initializeFromIntent(getIntent());
        barcodeView.decodeContinuous(callback);
        beepManager = new BeepManager(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();

        barcodeView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        barcodeView.pause();
    }

    @Override
    public void onBackPressed(){

    }
}
