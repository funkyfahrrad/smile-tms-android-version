package com.here.android.tutorial;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class LoginScreen extends AppCompatActivity {

    int status;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginreg);
        //PreferenceManager.getDefaultSharedPreferences(this).edit().putString("LastActivity","LoginScreen").apply();
        //PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("AlreadyRegistered",false).apply();
        String lastAct="";
        lastAct=PreferenceManager.getDefaultSharedPreferences(this).getString("LastActivity", "LoginScreen");
        //startService(new Intent(getBaseContext(),APITest2.class));
        assert lastAct != null : "lastAct is null";
        if (!lastAct.equals("LoginScreen")){
            lastAct="com.here.android.tutorial."+lastAct;
            Class resumeClass;
            try {
                resumeClass = Class.forName(lastAct);
                Intent intent=new Intent(LoginScreen.this,resumeClass);
                startActivity(intent);
                finish();
            }
            catch(ClassNotFoundException e){
                e.printStackTrace();
            }


        }
        String b="";
        Intent getInt=getIntent();
        if (!getInt.getBooleanExtra("success",true)){
            startService(new Intent(getBaseContext(),ServiceInternetConnection.class));
        }
        boolean alreadyReg=PreferenceManager.getDefaultSharedPreferences(this).getBoolean("AlreadyRegistered", false);
        if (alreadyReg){
            Button button=findViewById(R.id.btnreg);
            button.setText("Profil bearbeiten");
        }
        boolean completedTourSent=PreferenceManager.getDefaultSharedPreferences(this).getBoolean("CompletedTourSent", true);
        if (!completedTourSent){
            Button button=findViewById(R.id.btnlogin);
            button.setText("Tour abschließen");
        }
    }

    public void trylogin(View v){
        CheckInternetCon checkInternetCon=new CheckInternetCon();
        boolean connected=checkInternetCon.check(LoginScreen.this);
        if (connected){
            boolean completedTourSent=PreferenceManager.getDefaultSharedPreferences(this).getBoolean("CompletedTourSent", true);
            if (completedTourSent){
                loginProcedure();
            }
            else{
                Intent intent=new Intent(LoginScreen.this,RestCalls.class);
                startActivity(intent);
                finish();
            }
        }
        else{
            checkInternetCon.noInternet(LoginScreen.this);
        }



    }

    private void loginProcedure(){
        EditText e_name=findViewById(R.id.editname_login);
        EditText e_pw=findViewById(R.id.editpw_login);
        final String username=e_name.getText().toString();
        final String password=e_pw.getText().toString();

        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("alias", username);
            jsonObject.put("password", password);
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                "https://pickshare.herokuapp.com/auth",jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        String a="";
                        try{
                            String token=response.getString("token");
                        }
                        catch(JSONException e){
                            e.printStackTrace();
                        }
                        Intent intent=new Intent(LoginScreen.this,TourSuche.class);
                        startActivity(intent);
                        finish();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Result", "NoSuccess");
                Toast.makeText(LoginScreen.this,"Login fehlgeschlagen",Toast.LENGTH_LONG).show();
            }
        }){


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return super.getHeaders();
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                if (response.data.length == 0) {
                    byte[] responseData = "{}".getBytes(StandardCharsets.UTF_8);
                    status=response.statusCode;
                    response = new NetworkResponse(response.statusCode, responseData, response.headers, response.notModified);
                }

                return super.parseNetworkResponse(response);
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        //jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);



        /**/
    }

    public void regprocess(View v){
        //Intent intent = new Intent (LoginScreen.this, APITest.class);
        //Intent intent = new Intent (LoginScreen.this, DownloadMap.class);
        Intent intent=new Intent(LoginScreen.this,RegAccEmailPW.class);
        startActivity(intent);
        finish();
    }
}
