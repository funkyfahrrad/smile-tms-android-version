package com.here.android.tutorial;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class RegAccEmailPW extends AppCompatActivity {

    EditText email, password, repPassword;
    String Spassword, SrepPassword, Semail;
    Integer status;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registeraccount);
        email = findViewById(R.id.editemail);
        password = findViewById(R.id.editpw1);
        repPassword = findViewById(R.id.editpw2);
        boolean alreadyReg=PreferenceManager.getDefaultSharedPreferences(this).getBoolean("AlreadyRegistered", false);
        //startService(new Intent(getBaseContext(),APITest2.class));
        if (alreadyReg){
            Intent intent=new Intent(RegAccEmailPW.this,RegisterProcess.class);
            startActivity(intent);
            finish();
        }
    }


    public void regacc(View v) {
        status = 0;
        Spassword = password.getText().toString();
        SrepPassword = repPassword.getText().toString();
        Semail = email.getText().toString();
        if (!Semail.toString().equals("") && !Spassword.toString().equals("") && !SrepPassword.toString().equals("")) {
            if (Spassword.equals(SrepPassword)) {
                CheckInternetCon checkInternetCon=new CheckInternetCon();
                boolean check=checkInternetCon.check(RegAccEmailPW.this);
                if (check){
                    callApi();
                }
                else{
                    checkInternetCon.noInternet(RegAccEmailPW.this);
                }
            } else {
                Toast.makeText(RegAccEmailPW.this, "Passwörter stimmen nicht überein!", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(RegAccEmailPW.this, "Bitte alle Felder ausfüllen!", Toast.LENGTH_LONG).show();
        }


        //startActivity(intent);
        //finish();
    }


    protected void callApi() {
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("password", password.getText().toString());
            jsonObject.put("role", 3);
            jsonObject.put("email", email.getText().toString());
            jsonObject.put("inviter", "5ec5002102cbd20015130c76");
            jsonObject.put("firstName", "fName");
            jsonObject.put("lastName", "lName");
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                "https://pickshare.herokuapp.com/users/register",jsonObject,
        new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (status==201){
                    Log.i("Result", "Success");
                    registerSuccess();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Result", "UnSuccess");
                if(error.networkResponse.statusCode==409){
                    Toast.makeText(RegAccEmailPW.this,"Diese E-Mail Adresse wird bereits verwendet",Toast.LENGTH_LONG).show();
                }
            }
        }){


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return super.getHeaders();
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                if (response.data.length == 0) {
                    byte[] responseData = "{}".getBytes(StandardCharsets.UTF_8);
                    status=response.statusCode;
                    response = new NetworkResponse(response.statusCode, responseData, response.headers, response.notModified);
                }

                return super.parseNetworkResponse(response);
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        //jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void registerSuccess(){
        PreferenceManager.getDefaultSharedPreferences(RegAccEmailPW.this).edit().putBoolean("AlreadyRegistered",true).apply();
        new AlertDialog.Builder(RegAccEmailPW.this)
                .setTitle("Deine Anmeldedaten wurden erfolgreich übermittelt!")
                .setMessage("Bitte ergänze jetzt dein Fahrerprofil")

                .setPositiveButton("Fortfahren", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(RegAccEmailPW.this, RegisterProcess.class);
                        intent.putExtra("email", email.getText().toString());
                        startActivity(intent);
                        finish();
                    }

                })

                .show();
    }
}
