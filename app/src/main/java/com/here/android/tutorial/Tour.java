package com.here.android.tutorial;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.here.android.mpa.common.GeoCoordinate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Tour extends AppCompatActivity {

    Button gesamt, weiter;
    String tourinfo,starttime,coorddepot;
    TextView depottv;
    JSONObject depotinfo;
    JSONArray tourdata;
    ArrayList<String> coordinates=new ArrayList<String>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tour);
        depottv=findViewById(R.id.tourinfotxt);
        DatabaseHandler db=new DatabaseHandler(this);
        List<DB_CONTENT_REC> depotList=db.getDepot();
        String streetName="", streetNumber="", zip="", city="";
        for (DB_CONTENT_REC dcr : depotList){
            starttime=dcr.getSTIME();
            streetName=dcr.getSTRNAME();
            streetNumber=dcr.getSTRNMBR();
            zip=dcr.getZIP();
            city=dcr.getCITY();
        }
        String depottxt="Depot\n"+streetName+" "+streetNumber+"\n"+zip+" "+city+"\n Geplante Zeit: "+starttime;
        depottv.setText(depottxt);
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("LastActivity","Tour").apply();
        Intent getintent=getIntent();
        //starttime=getintent.getStringExtra("time");
        //starttime="17:10";
        /*
        tourinfo=getintent.getStringExtra("tourinfo");
        depottv=findViewById(R.id.tourinfotxt);
        try {
            JSONObject tour = new JSONObject(tourinfo);
            JSONObject timearray=tour.getJSONObject("tourMetaData");
            starttime=timearray.getString("tourStartTime");
            starttime=starttime.substring(starttime.length()-5);
            JSONArray tourarray=tour.getJSONArray("stops");
            tourdata=tourarray;
            int delete=0;
            for (int i=0;i<tourarray.length();i++){
                JSONObject currentObject=tourarray.getJSONObject(i);
                String latitude=currentObject.getString("latitude");
                String longitude=currentObject.getString("longitude");
                String c=latitude+"?"+longitude;
                coordinates.add(c);
                if (currentObject.getString("stopType").equals("Depot")){
                    depotinfo=currentObject;
                    delete=i;
                    String depottxt="Depot\n"+currentObject.getString("streetName")+" "+currentObject.getString("streetNumber")+"\n"+currentObject.getString("zip")+" "+currentObject.getString("city")+"\n Geplante Zeit: "+starttime;
                    depottv.setText(depottxt);
                    coorddepot=currentObject.getString("latitude")+"?"+currentObject.getString("longitude");
                }
            }
            tourdata.remove(delete);
        }
        catch (JSONException e){
            Log.e("Tour", e.toString());
        }
        */
    }

    public void showtour(View v){

        Intent intent=new Intent(Tour.this,GesamteTour.class);
        startActivity(intent);
    }

    public void tourprog(View v){
        Intent intent=new Intent(Tour.this,QrCodeScan.class);
        intent.putExtra("tourinfos",tourinfo);
        startActivity(intent);
        finish();
    }

    public void navdepot(View v){
        Intent intent=new Intent(Tour.this,Navigation.class);
        intent.putExtra("coordinates",coorddepot);
        startActivity(intent);
    }
}
