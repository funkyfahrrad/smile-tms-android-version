package com.here.android.tutorial;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;


import com.here.android.mpa.common.GeoCoordinate;

import java.util.ArrayList;
import java.util.List;

public class GesamteTour extends AppCompatActivity {

    private gestour m_mapFragmentView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gesamtetour);
        ArrayList<String> coordinates=new ArrayList<String>();
        DatabaseHandler db=new DatabaseHandler(this);
        List<DB_CONTENT_REC> dblist=db.getAllCoordinates();
        for (DB_CONTENT_REC dcr : dblist){
            String coordinate=dcr.getLAT()+"?"+dcr.getLONG();
            coordinates.add(coordinate);
        }
        if (m_mapFragmentView==null){
            m_mapFragmentView = new gestour(GesamteTour.this, coordinates);}
    }

    public void gesback(View v){
        finish();
    }

}
