package com.here.android.tutorial;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.guidance.NavigationManager;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.mapping.SupportMapFragment;
import com.here.android.mpa.routing.CoreRouter;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.routing.RouteWaypoint;
import com.here.android.mpa.routing.Router;
import com.here.android.mpa.routing.RoutingError;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.List;

import static com.here.android.mpa.common.OnEngineInitListener.Error.NONE;

public class MapFragmentView {

    private SupportMapFragment m_mapFragment;
    private Button m_createRouteButton;
    private AppCompatActivity m_activity;
    private Map m_map;
    private Button m_naviControlButton;
    private MapRoute m_mapRoute;
    private Route m_route;
    private boolean m_foregroundServiceStarted;
    private NavigationManager m_navigationManager;
    private GeoBoundingBox m_geoBoundingBox;
    private PositioningManager pmanager;
    Double latitude,longitude;

    public MapFragmentView(AppCompatActivity activity, String coordinates) {
        m_activity = activity;
        /*if (step==1){
            latitude=51.335453;
            longitude=12.356197;
        }
        if (step==2){
            latitude=51.335410;
            longitude=12.383487;
        }
        if (step==3){

            latitude=51.348767;
            longitude=12.373810;,,
        }*/
        String longlat[]=coordinates.split("\\?");
        latitude=Double.parseDouble(longlat[0]);
        longitude=Double.parseDouble(longlat[1]);
       /* if (step==1){
            latitude=52.513477;
            longitude=13.304112;
        }
        if (step==2){
            latitude=52.513588;
            longitude=13.301398;
        }
        if (step==3){
            latitude=52.514848;
            longitude=13.305663;
        }*/
        initMapFragment();
        /*
         * We use a button in this example to control the route calculation
         */
        //initCreateRouteButton();
        //initNaviControlButton();
    }

    private SupportMapFragment getMapFragment() {
        return (SupportMapFragment) m_activity.getSupportFragmentManager().findFragmentById(R.id.mapfragment);
    }

    private void initMapFragment() {
        /* Locate the mapFragment UI element */
        m_mapFragment = getMapFragment();

        // Set path of isolated disk cache
        String diskCacheRoot = Environment.getExternalStorageDirectory().getPath()
                + File.separator + ".isolated-here-maps";
        // Retrieve intent name from manifest
        String intentName = "";
        try {
            ApplicationInfo ai = m_activity.getPackageManager().getApplicationInfo(m_activity.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            intentName = bundle.getString("INTENT_NAME");
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(this.getClass().toString(), "Failed to find intent name, NameNotFound: " + e.getMessage());
        }

        //boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(diskCacheRoot, intentName);
        boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(
                diskCacheRoot,
                "com.here.android.tutorial.MapService");

        if (!success) {
            // Setting the isolated disk cache was not successful, please check if the path is valid and
            // ensure that it does not match the default location
            // (getExternalStorageDirectory()/.here-maps).
            // Also, ensure the provided intent name does not match the default intent name.
        } else {
            if (m_mapFragment != null) {
                /* Initialize the SupportMapFragment results will be given via the called back. */
                m_mapFragment.init(new OnEngineInitListener() {
                    @Override
                    public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {

                        if (error == NONE) {
                            /* get the map object */
                            m_map = m_mapFragment.getMap();

                            /*
                             * Set the map center to the 4350 Still Creek Dr Burnaby BC (no animation).
                             */
                            m_map.setCenter(new GeoCoordinate(51.3377916, 12.3767061, 0.0),
                                    Map.Animation.NONE);

                            /*
                            * Show position on rendered map, function deprecated TODO
                            * */
                            m_map.getPositionIndicator().setVisible(true);
                            /* Set the zoom level to the average between min and max zoom level. */
                            m_map.setZoomLevel((m_map.getMaxZoomLevel() + m_map.getMinZoomLevel()) / 2);
                            m_navigationManager = NavigationManager.getInstance();
                            pmanager=PositioningManager.getInstance();
                            pmanager.start(PositioningManager.LocationMethod.GPS_NETWORK);
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    GeoPosition test=pmanager.getPosition();
                                    //GeoCoordinate fc=test.getCoordinate();
                                    createRoute();
                                }
                            }, 5000);
                        } else {
                            Toast.makeText(m_activity,
                                    "ERROR: Cannot initialize Map with error " + error,
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        }

    }

    private void initCreateRouteButton() {
        //m_createRouteButton = (Button) m_activity.findViewById(R.id.button);

        m_createRouteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                 * Clear map if previous results are still on map,otherwise proceed to creating
                 * route
                 */
                if (m_map != null && m_mapRoute != null) {
                    m_map.removeMapObject(m_mapRoute);
                    m_mapRoute = null;
                } else {
                    /*
                     * The route calculation requires local map data.Unless there is pre-downloaded
                     * map data on device by utilizing MapLoader APIs, it's not recommended to
                     * trigger the route calculation immediately after the MapEngine is
                     * initialized.The INSUFFICIENT_MAP_DATA error code may be returned by
                     * CoreRouter in this case.
                     *
                     */
                    createRoute();
                }
            }
        });

    }



    /* Creates a route from 4350 Still Creek Dr to Langley BC with highways disallowed */
    private void createRoute() {
        /*RoutePlan routePlan=new RoutePlan();
        GeoCoordinate rw1=new GeoCoordinate(51.335453,12.356197);
        GeoCoordinate rw2=new GeoCoordinate(51.344760,12.380221);
        routePlan.addWaypoint(new RouteWaypoint(rw1));
        routePlan.addWaypoint(new RouteWaypoint(rw2));

        //51.335453,12.356197
        //51.344760,12.380221
        GeoCoordinate gc=new GeoCoordinate(51.343479, 12.387772);
        GeoBoundingBox box=new GeoBoundingBox(gc,5000,5000);
        MapDataPrefetcher.Request request = MapDataPrefetcher.getInstance( ).fetchMapData(box);

        /* Initialize a CoreRouter */
        //PositioningManager.getInstance().start(PositioningManager.LocationMethod.GPS_NETWORK);
        CoreRouter coreRouter = new CoreRouter();

        /* Initialize a RoutePlan */
        RoutePlan routePlan = new RoutePlan();

        /*
         * Initialize a RouteOption.HERE SDK allow users to define their own parameters for the
         * route calculation,including transport modes,route types and route restrictions etc.Please
         * refer to API doc for full list of APIs
         */
        RouteOptions routeOptions = new RouteOptions();
        /* Other transport modes are also available e.g Pedestrian */
        routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
        /* Disable highway in this route. */
        routeOptions.setHighwaysAllowed(false);
        /* Calculate the shortest route available. */
        routeOptions.setRouteType(RouteOptions.Type.SHORTEST);
        /* Calculate 1 route. */
        routeOptions.setRouteCount(1);
        /* Finally set the route option */
        routePlan.setRouteOptions(routeOptions);

        //Coordinates for current position
        GeoPosition g=pmanager.getPosition();
        Double herelat=g.getCoordinate().getLatitude();
        Double herelong=g.getCoordinate().getLongitude();
        GeoCoordinate start=g.getCoordinate();

        //set way points
        RouteWaypoint here=new RouteWaypoint(start);
        //GeoCoordinate sp=new GeoCoordinate(pmanager.start(PositioningManager.LocationMethod.GPS));
        //RouteWaypoint startPoint = new RouteWaypoint(new GeoCoordinate(51.335453, 12.356197));
        /* END: Langley BC */
        RouteWaypoint destination = new RouteWaypoint(new GeoCoordinate(latitude, longitude));
        /*GeoCoordinate test1= new GeoCoordinate(51.360196, 12.374120, 0.0);
        GeoCoordinate test2=new GeoCoordinate(latitude,longitude);
        Boolean test1b=test1.isValid();
        Boolean test2b=test2.isValid();
        Boolean test3b=start.isValid();*/

        /* Add both waypoints to the route plan */
        routePlan.addWaypoint(here);
        // routePlan.addWaypoint(startPoint);
        routePlan.addWaypoint(destination);

        /* Trigger route calculation,results will be called back via the listener */
        coreRouter.calculateRoute(routePlan,
                new Router.Listener<List<RouteResult>, RoutingError>() {
                    @Override
                    public void onProgress(int i) {
                        /* calculation progress */
                    }

                    @Override
                    public void onCalculateRouteFinished(List<RouteResult> routeResults,
                                                         RoutingError routingError) {
                        if (routingError == RoutingError.NONE) {
                            if (routeResults.get(0).getRoute() != null) {
                                /* Create a MapRoute */
                                m_route = routeResults.get(0).getRoute();
                                m_mapRoute = new MapRoute(routeResults.get(0).getRoute());

                                /* Show the maneuver number on top of route */
                                m_mapRoute.setManeuverNumberVisible(true);
                                m_geoBoundingBox = routeResults.get(0).getRoute().getBoundingBox();

                                /* Add the MapRoute to the map */
                                m_map.addMapObject(m_mapRoute);

                                /*
                                 * We may also want to make sure the map view is orientated properly
                                 * so the entire route can be easily seen.
                                 */
                                GeoBoundingBox gbb = routeResults.get(0).getRoute()
                                        .getBoundingBox();
                                m_map.zoomTo(gbb, Map.Animation.NONE,
                                        Map.MOVE_PRESERVE_ORIENTATION);
                                startNavigation();
                            } else {
                                Toast.makeText(m_activity,
                                        "Error:route results returned is not valid",
                                        Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(m_activity,
                                    "Error:route calculation returned error code: " + routingError,
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });

    }




    private void initNaviControlButton() {
        //m_naviControlButton = (Button) m_activity.findViewById(R.id.naviCtrlButton);
        m_naviControlButton.setText("Start Navi");
        m_naviControlButton.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {

                if (m_route == null) {
                    createRoute();
                } else {
                    m_navigationManager.stop();
                    /*
                     * Restore the map orientation to show entire route on screen
                     */
                    m_map.zoomTo(m_geoBoundingBox, Map.Animation.NONE, 0f);
                    m_naviControlButton.setText("Start Navi");
                    m_route = null;
                }
            }
        });
    }


    private void startForegroundService() {
        if (!m_foregroundServiceStarted) {
            m_foregroundServiceStarted = true;
            Intent startIntent = new Intent(m_activity, ForegroundService.class);
            startIntent.setAction(ForegroundService.START_ACTION);
            m_activity.getApplicationContext().startService(startIntent);
        }
    }

    private void stopForegroundService() {
        if (m_foregroundServiceStarted) {
            m_foregroundServiceStarted = false;
            Intent stopIntent = new Intent(m_activity, ForegroundService.class);
            stopIntent.setAction(ForegroundService.STOP_ACTION);
            m_activity.getApplicationContext().startService(stopIntent);
        }
    }

    private void startNavigation() {
        //m_naviControlButton.setText("STOPP");
        /* Configure Navigation manager to launch navigation on current map */
        m_navigationManager.setMap(m_map);

        /*
         * Start the turn-by-turn navigation.Please note if the transport mode of the passed-in
         * route is pedestrian, the NavigationManager automatically triggers the guidance which is
         * suitable for walking. Simulation and tracking modes can also be launched at this moment
         * by calling either simulate() or startTracking()
         */

        /* Choose navigation modes between real time navigation and simulation */
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(m_activity);
        alertDialogBuilder.setTitle("Navigation");
        alertDialogBuilder.setMessage("Choose Mode");
        alertDialogBuilder.setNegativeButton("Navigation",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                m_navigationManager.startNavigation(m_route);
                m_map.setTilt(60);
                startForegroundService();
            };
        });
        alertDialogBuilder.setPositiveButton("Simulation",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                m_navigationManager.simulate(m_route,200);//Simualtion speed is set to 200 m/s
                m_map.setTilt(60);
                startForegroundService();
            };
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        /*
         * Set the map update mode to ROADVIEW.This will enable the automatic map movement based on
         * the current location.If user gestures are expected during the navigation, it's
         * recommended to set the map update mode to NONE first. Other supported update mode can be
         * found in HERE Android SDK API doc
         */
        m_navigationManager.setMapUpdateMode(NavigationManager.MapUpdateMode.ROADVIEW);

        /*
         * NavigationManager contains a number of listeners which we can use to monitor the
         * navigation status and getting relevant instructions.In this example, we will add 2
         * listeners for demo purpose,please refer to HERE Android SDK API documentation for details
         */
        addNavigationListeners();
    }

    private void addNavigationListeners() {

        /*
         * Register a NavigationManagerEventListener to monitor the status change on
         * NavigationManager
         */
        m_navigationManager.addNavigationManagerEventListener(
                new WeakReference<NavigationManager.NavigationManagerEventListener>(
                        m_navigationManagerEventListener));

        /* Register a PositionListener to monitor the position updates */
        m_navigationManager.addPositionListener(
                new WeakReference<NavigationManager.PositionListener>(m_positionListener));
    }

    private NavigationManager.PositionListener m_positionListener = new NavigationManager.PositionListener() {
        @Override
        public void onPositionUpdated(GeoPosition geoPosition) {
            /* Current position information can be retrieved in this callback */
        }
    };

    private NavigationManager.NavigationManagerEventListener m_navigationManagerEventListener = new NavigationManager.NavigationManagerEventListener() {
        @Override
        public void onRunningStateChanged() {
            Toast.makeText(m_activity, "rstatechange", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNavigationModeChanged() {
            Toast.makeText(m_activity, "nmodechange", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onEnded(NavigationManager.NavigationMode navigationMode) {
            Toast.makeText(m_activity, "was ended", Toast.LENGTH_SHORT).show();
            stopForegroundService();
        }

        @Override
        public void onMapUpdateModeChanged(NavigationManager.MapUpdateMode mapUpdateMode) {
            Toast.makeText(m_activity, "umodechange",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRouteUpdated(Route route) {
            Toast.makeText(m_activity, "Route updated", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCountryInfo(String s, String s1) {
            Toast.makeText(m_activity, "Country info updated from " + s + " to " + s1,
                    Toast.LENGTH_SHORT).show();
        }
    };

    public void onDestroy() {
        /* Stop the navigation when app is destroyed */
        if (m_navigationManager != null) {
            m_navigationManager.stop();
        }
        if (pmanager!=null){
            pmanager.stop();
        }
        stopForegroundService();
    }

    public void destroy() {
        /* Stop the navigation when app is destroyed */
        if (m_navigationManager != null) {
            m_navigationManager.stop();
        }
        if (pmanager!=null){
            pmanager.stop();
        }
        stopForegroundService();
    }




}