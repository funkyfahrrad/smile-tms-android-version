package com.here.android.tutorial;

import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.GeocodeResult;
import com.here.android.mpa.search.ResultListener;

import java.util.List;

public class GeocodeListener implements ResultListener<List<GeocodeResult>> {
    @Override
    public void onCompleted(List<GeocodeResult> data, ErrorCode error) {
        if (error != ErrorCode.NONE) {
            // Handle error

        } else {
            // Process result data

        }
    }
}
