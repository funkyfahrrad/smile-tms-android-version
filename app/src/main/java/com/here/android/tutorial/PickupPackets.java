package com.here.android.tutorial;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.journeyapps.barcodescanner.DefaultDecoderFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PickupPackets extends Activity {

    private DecoratedBarcodeView barcodeView;
    private BeepManager beepManager;
    private String lastText,tourdata;
    private ArrayList<String> ssccList=new ArrayList<String>();
    private ArrayList<String> scannedssccs=new ArrayList<String>();
    private Integer check;
    private ListView ssccLV;
    private ArrayAdapter<String> adapter;

    JSONArray jsonTours;

    SimpleDateFormat currentDay=new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    SimpleDateFormat currentTime=new SimpleDateFormat("HH:mm:ss.000Z",Locale.getDefault());


    private BarcodeCallback callback=new BarcodeCallback(){
        @Override
        public void barcodeResult(BarcodeResult result){
            if(result.getText() == null || result.getText().equals(lastText)) {
                // Prevent duplicate scans
                return;
            }
            check=0;
            lastText = result.getText();
            //tv.setText(lastText);
            if (scannedssccs.contains(lastText)){
                Toast.makeText(PickupPackets.this,"Packet wurde bereits registriert",Toast.LENGTH_LONG).show();
                check=1;
            }
            if (ssccList.contains(lastText)){
                Toast.makeText(PickupPackets.this,"Packet wurde erfolgreich registriert",Toast.LENGTH_LONG).show();
                scannedssccs.add(lastText);
                ssccList.remove(lastText);
                String cTime=currentDay.format(new Date())+"T"+currentTime.format(new Date());
                final DatabaseHandler db=new DatabaseHandler(PickupPackets.this);
                db.updatePickupTime(lastText,cTime);
                adapter.notifyDataSetChanged();
                check=1;
            }
            if(check==0){
                Toast.makeText(PickupPackets.this,"Packet nicht in Tour enthalten",Toast.LENGTH_LONG).show();
            }
            if (ssccList.size()==0){
                new AlertDialog.Builder(PickupPackets.this)
                        .setTitle("Alle Pakete wurden erfolgreich eingescannt!")
                        .setMessage("Du kannst jetzt mit deiner Tour starten!")

                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finishPickup();
                            }

                        })

                        .show();
            }
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints){

        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pickuppaks_lo);
        ssccLV=findViewById(R.id.ssccListView);
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("LastActivity","PickupPackets").apply();
        AndroidNetworking.initialize(getApplicationContext());
        DatabaseHandler db=new DatabaseHandler(this);
        List<DB_CONTENT_PAK> dbContentPak=db.getPickupPackets();
        for (DB_CONTENT_PAK dpr : dbContentPak){
            ssccList.add(dpr.getSSCC());
        }
        adapter=new ArrayAdapter<String>(PickupPackets.this, R.layout.sscclistscan,ssccList);
        ssccLV.setAdapter(adapter);
        //tv=findViewById(R.id.textviewbarcode);

        barcodeView=findViewById(R.id.barcode_scanner);
        Collection<BarcodeFormat> formats= Arrays.asList(BarcodeFormat.QR_CODE,BarcodeFormat.CODE_39);
        barcodeView.getBarcodeView().setDecoderFactory(new DefaultDecoderFactory(formats));
        barcodeView.initializeFromIntent(getIntent());
        barcodeView.decodeContinuous(callback);
        beepManager = new BeepManager(this);
    }


    @Override
    protected void onResume() {
        super.onResume();

        barcodeView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        barcodeView.pause();
    }

    public void pause(View view) {
        barcodeView.pause();
    }

    public void resume(View view) {
        barcodeView.resume();
    }

    public void triggerScan(View view) {
        barcodeView.decodeSingle(callback);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    public void finishPickup(){
        /*SimpleDateFormat currentDay=new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        SimpleDateFormat currentTime=new SimpleDateFormat("HH:mm:ss.000Z",Locale.getDefault());
        String cTime=currentDay.format(new Date())+"T"+currentTime.format(new Date());
        final DatabaseHandler db=new DatabaseHandler(PickupPackets.this);
        while (scannedssccs.size()>0){
            String sscc=scannedssccs.get(0);
            //sscc="urn:epc:id:sscc:"+sscc;
            //sscc=sscc.substring(0,sscc.length()-10)+"."+sscc.substring(sscc.length()-10);
            db.updatePickupTime(sscc,cTime);
            db.updatePickup(new DB_CONTENT_PAK(1,sscc));//WENN API CALL ERFOLGREICH
            scannedssccs.remove(0);

        }*/
       // https://bpt-lab.org/smile/caz/tms/pick-up-reported
        Intent intent=new Intent(PickupPackets.this,RestCalls.class);
        intent.putExtra("call","pickup");
        //intent.putExtra("tourinfos",tourdata);
        startActivity(intent);
        finish();
    }


}
