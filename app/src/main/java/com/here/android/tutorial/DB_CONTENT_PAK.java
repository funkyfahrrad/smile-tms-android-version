package com.here.android.tutorial;

/**
 * Created by micha on 14.11.2017.
 */

public class DB_CONTENT_PAK {

    int _id;
    String _sscc;
    int _receiverid;
    String _pickdate;
    String _receivedate;
    int _pickup;
    int _delivery;
    int _scanned;
    int _sigsent;

    public DB_CONTENT_PAK(){

    }

    public DB_CONTENT_PAK(int id, String sscc, int receiverid, String pickdate, String receivedate, int scanned, int sigsent){
        this._id= id;
        this._sscc=sscc;
        this._receiverid=receiverid;
        this._receivedate=receivedate;
        this._pickdate=pickdate;
        this._scanned=scanned;
        this._sigsent=sigsent;
    }

    public DB_CONTENT_PAK(int pickup, String sscc){
        this._pickup=pickup;
        this._sscc=sscc;
    }

    public DB_CONTENT_PAK(String sscc, int receiverid, String pickdate, String receivedate, int pickup, int delivery, int scanned, int sigsent){
        this._sscc=sscc;
        this._receiverid=receiverid;
        this._receivedate=receivedate;
        this._pickdate=pickdate;
        this._pickup=pickup;
        this._delivery=delivery;
        this._scanned=scanned;
        this._sigsent=sigsent;
    }

    public int getID(){
        return this._id;
    }

    public void setID(int id){ this._id=id; }

    public String getSSCC(){return this._sscc;}

    public void setSSCC(String sscc){this._sscc=sscc;}

    public int getRECID(){return this._receiverid;}

    public void setRECID(int recid){this._receiverid=recid;}

    public String getRECDATE(){
        return this._receivedate;
    }

    public void setRECDATE(String recdate){
        this._receivedate=recdate;
    }

    public String getPICKDATE(){
        return this._pickdate;
    }

    public void setPICKDATE(String pickdate){
        this._pickdate=pickdate;
    }

    public Integer getPICKUP(){
        return this._pickup;
    }

    public void setPICKUP(int pickup){
        this._pickup=pickup;
    }

    public Integer getDELIVERY(){
        return this._delivery;
    }

    public void setDELIVERY(int delivery){
        this._delivery=delivery;
    }

    public int getSCANNED(){
        return this._scanned;
    }

    public void setSCANNED(int scanned){
        this._scanned=scanned;
    }

    public Integer getSIGSENT(){return this._sigsent;}

    public void setSIGSENT(Integer sigsent){this._sigsent=sigsent;}
}
