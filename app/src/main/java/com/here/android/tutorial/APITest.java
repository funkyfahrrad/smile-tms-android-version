package com.here.android.tutorial;

import android.accounts.NetworkErrorException;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.service.autofill.Transformation;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.exception.ApolloHttpException;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.here.android.tutorial.type.TimeInWeekInput;
import com.here.android.tutorial.type.WishTimeframeInput;
import com.itkacher.okhttpprofiler.OkHttpProfilerInterceptor;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Route;

import androidx.annotation.Nullable;

import static com.here.android.tutorial.type.Weekday.FRIDAY;
import static com.here.android.tutorial.type.Weekday.MONDAY;
import static com.here.android.tutorial.type.Weekday.SATURDAY;
import static com.here.android.tutorial.type.Weekday.THURSDAY;
import static com.here.android.tutorial.type.Weekday.TUESDAY;
import static com.here.android.tutorial.type.Weekday.WEDNESDAY;

public class APITest extends AppCompatActivity {
    TextView t1,t2;
    JSONObject testjson;
    Date date;
    String test,s;
    ApolloClient apolloClient;
    Integer responsec;

    LinearLayout pData_lo, vData_lo, rData_lo, tData_lo;
    int height, height1, height2;
    EditText movon,mobis;

    private IntentIntegrator qrScanI;
    private static final String TAG = "APITest";



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.apitest);
        apolloClient=ApolloClient.builder()
                .serverUrl("https://smile.goodstag.com/graphql")
                .okHttpClient(
                        new OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS)
                                .writeTimeout(30, TimeUnit.SECONDS)
                                .readTimeout(30, TimeUnit.SECONDS)
                                .addInterceptor(new OkHttpProfilerInterceptor())
                                .authenticator(new Authenticator() {
                                    @androidx.annotation.Nullable
                                    @Override
                                    public Request authenticate(@androidx.annotation.Nullable Route route, okhttp3.Response response) throws IOException {
                                        String credentials= Credentials.basic("smile","sei7ieQuueka");
                                        return response.request().newBuilder().header("Authorization",credentials).build();
                                    }
                                })
                                .build()

                ).build();
        CheckInternetCon ci=new CheckInternetCon();
        boolean internet= ci.check(this);
        if (!internet){
            AlertDialog alertDialog=ci.noInternet(this);
            alertDialog.show();
        }

    }


    public void qrscan(View v){
        int costsPerH=5;
        int costsPerKm=4;
        int costsPerStop=2;
        ArrayList<TimeInWeekInput> cutOffTimes=cutoff();
        String firstname="Thomaste";
        String lastname="Test";
        List<String> deliveryAreas=new ArrayList<>();
        deliveryAreas.add("51829");//TODO Aufteilen!
        Float delCapacatiyVol=0.0f;
        Float  delCapacityWeight=50.3f;
        int maxPackets=50;
        int maxStopsPTour=15;  //TODO ???
        String tmsCarrieId="0123"; //PLACEHOLDER
        String vehicType="Bicycle";
        List<WishTimeframeInput> wTimes=Timeframes();
        CreateDelMutation delMutation= CreateDelMutation.builder().costsPerHour(costsPerH).costsPerKm(costsPerKm).costsPerStop(costsPerStop).cufOffTimes(cutOffTimes)
                .delivererFirstName(firstname).delivererLastName(lastname).deliveryAreas(deliveryAreas).deliveryCapacityVolume(delCapacatiyVol).deliveryCapacityWeight(delCapacityWeight)
                .maxPacketnumberPerTour(maxPackets).maxStopsPerTour(maxStopsPTour).tmsCarrierId(tmsCarrieId).vehicleType(vehicType).wishTimeframes(wTimes).build();

        apolloClient
                .mutate(delMutation)
                .enqueue(
                        new ApolloCall.Callback<CreateDelMutation.Data>() {
                            @Override
                            public void onResponse(@NotNull Response<CreateDelMutation.Data> response) {
                                Log.i(TAG, response.toString());
                                test=response.toString();
                                test=response.toString();
                            }

                            @Override
                            public void onFailure(@NotNull ApolloException e) {
                                Log.e(TAG, e.getMessage(), e);
                                test=e.toString();
                                test=e.toString();
                            }
                        }
                );
    }

    private ArrayList<TimeInWeekInput> cutoff(){
        ArrayList<TimeInWeekInput> cutoff=new ArrayList<>();
        //TODO
        TimeInWeekInput tiW=TimeInWeekInput.builder().day(MONDAY).hour(12).minute(0).build();
        cutoff.add(tiW);


        return cutoff;
    }

    private ArrayList<WishTimeframeInput> Timeframes(){
        JSONArray timef=new JSONArray();
        ArrayList<WishTimeframeInput> wti= new ArrayList<>();

            int start=12;
            int end=16;
            WishTimeframeInput wt= WishTimeframeInput.builder().day(MONDAY).startTime(start).endTime(end).build();
            wti.add(wt);


            int start2=10;
            int end2=18;
            WishTimeframeInput wt2= WishTimeframeInput.builder().day(TUESDAY).startTime(start2).endTime(end2).build();
            wti.add(wt2);

        return wti;
    }
        //qrScanI.initiateScan();

        //SignatureMutation signatureMutation=SignatureMutation.builder().packetID(packetid).sig("testing").build();
       /* apolloClient
                .mutate(createDelMutation)
                .enqueue(
                        new ApolloCall.Callback<CreateDelMutation.Data>() {
                            @Override
                            public void onResponse(@NotNull Response<CreateDelMutation.Data> response) {
                                Log.i(TAG, response.toString());
                                test=response.toString();
                                test=response.toString();
                            }

                            @Override
                            public void onFailure(@NotNull ApolloException e) {
                                Log.e(TAG, e.getMessage(), e);
                                test=e.toString();
                                test=e.toString();
                            }
                        }
    );*/

    /*    apolloClient
                .mutate(signatureMutation)
                .enqueue(
                        new ApolloCall.Callback<SignatureMutation.Data>() {
                            @Override
                            public void onResponse(@NotNull Response<SignatureMutation.Data> response) {
                                Log.i(TAG, response.toString());
                                test=response.toString();
                                test=response.toString();
                            }

                            @Override
                            public void onFailure(@NotNull ApolloException e) {
                                Log.e(TAG, e.getMessage(), e);
                                test=e.toString();
                                test=e.toString();
                            }
                        }
                );*/
    //final PacketQuery packetQuery= PacketQuery.builder().sscc("urn:epc:id:sscc:4236537.0000000010").build();
        /*  TEST GRAPHQL
    UUID packetid=UUID.fromString("4e52314d-f71c-4793-9923-e3005120dc6b");
    CreateDelMutation createDelMutation=CreateDelMutation.builder().tmsCarrierId("1236").build();
    final String pakId="android2";
    final SignatureMutation signatureMutation=SignatureMutation.builder().packetID("5f11e58a-5b03-4a61-96e6-338a3e1a56c5").sig(pakId).build();
    apolloClient.mutate(signatureMutation).enqueue(new ApolloCall.Callback<SignatureMutation.Data>() {
        @Override
        public void onResponse(@NotNull Response<SignatureMutation.Data> response) {
            Log.i(TAG, response.toString());
            test=response.toString();
            test=response.toString();
        }

        @Override
        public void onFailure(@NotNull ApolloException e) {
            Log.e(TAG, e.getMessage(), e);
            test=e.toString();
            test=e.toString();
        }
    });
*/



    public void test(View v){
        String test="42365370000000010";
        SimpleDateFormat currentDay=new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        SimpleDateFormat currentTime=new SimpleDateFormat("HH:mm:ss.000Z",Locale.getDefault());
        String cTime=currentDay.format(new Date())+"T"+currentTime.format(new Date());
        //String sscc=test;
        String sscc="urn:epc:id:sscc:"+test;
        sscc=sscc.substring(0,sscc.length()-10)+"."+sscc.substring(sscc.length()-10);
        final DatabaseHandler db=new DatabaseHandler(APITest.this);
        //db.updatePickup(new DB_CONTENT_PAK(1,sscc));
        String a="";
        /*RequestParams requestParams=new RequestParams();
        requestParams.put("sscc",sscc);
        requestParams.put("pickDate",cTime);
        AsyncHttpClient client = new AsyncHttpClient();
        client.setBasicAuth(getApplicationContext().getResources().getString(R.string.gtapiuser), getApplicationContext().getResources().getString(R.string.gtapipass));
        client.post("https://bpt-lab.org/smile/caz/tms/pick-up-reported", requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                int i=statusCode;
                try{s=new String(responseBody,"UTF-8");}
                catch (UnsupportedEncodingException e){e.printStackTrace();}

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                int i=statusCode;
                try{s=new String(responseBody,"UTF-8");}
                catch (UnsupportedEncodingException e){e.printStackTrace();}
            }
        });*/
    }

}
